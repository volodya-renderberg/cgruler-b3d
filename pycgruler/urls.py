# -*- coding: utf-8 -*-

"""Url-ы """

try:
    import settings
except:
    from . import settings

def _join_kwargs(kw: dict) -> str:
    """Создаёт строку запроса из словаря: ``?key1=value1&key2=value2&...`` """
    path='?'
    for i, item in enumerate(kw.items()):
        if i==0:
            path=f"{path}{item[0]}={item[1]}"
        else:
            path=f"{path}&{item[0]}={item[1]}"
    return path

def api_login():
    """Путь до *main_app:api_login*"""
    return f'{settings.SERVER_URL}api/login/'


def studios_list():
    """Путь до *main_app:studios_list*"""
    return f'{settings.SERVER_URL}api/studios/'


def projects_list(studio_name):
    """Путь до *main_app:projects_list*"""
    return f'{settings.SERVER_URL}api/{studio_name}/projects/'


def assigned_tasks(studio_name):
    """Путь до *main_app:assigned_tasks*"""
    return f'{settings.SERVER_URL}api/{studio_name}/assigned_tasks/'


def checked_tasks(studio_name):
    """Путь до *main_app:checked_tasks*"""
    return f'{settings.SERVER_URL}api/{studio_name}/checked_tasks/'

def container_types(studio_name):
    """Путь до *main_app:container_types*"""
    return f'{settings.SERVER_URL}api/{studio_name}/container_types/'


def containers(studio_name):
    """Путь до *main_app:containers_list*"""
    return f'{settings.SERVER_URL}api/{studio_name}/containers/'


def container(studio_name, id):
    """Путь до *main_app:container_data*"""
    return f'{settings.SERVER_URL}api/{studio_name}/containers/{id}/'


def container_update(studio_name, id):
    """Путь до *main_app:container_data*"""
    return f'{settings.SERVER_URL}api/{studio_name}/containers/{id}/update/'


def versions(studio_name, **kw):
    """Путь до *main_app:versions* """
    if kw:
        path=f'{settings.SERVER_URL}api/{studio_name}/versions/{_join_kwargs(kw)}'
    else:
        path=f'{settings.SERVER_URL}api/{studio_name}/versions/'
    return path


def version(studio_name, id):
    """Путь до *main_app:version_data* """
    return f'{settings.SERVER_URL}api/{studio_name}/versions/{id}/'


def version_new(studio_name):
    """Путь до *main_app:version_new* """
    return f'{settings.SERVER_URL}api/{studio_name}/versions/new/'


def version_component_new(studio_name):
    """Путь до *main_app:version_component_new* """
    return f'{settings.SERVER_URL}api/{studio_name}/version_components/new/'


def version_types(studio_name, **kw):
    """Путь до. *main_app:version_types*"""
    if kw:
        path=f'{settings.SERVER_URL}api/{studio_name}/version_types/{_join_kwargs(kw)}'
    else:
        path=f'{settings.SERVER_URL}api/{studio_name}/version_types/'
    return path


def working_status_types():
    """Путь до *main_app:working_status_types_list*"""
    return f'{settings.SERVER_URL}api/working_status_types/'


def working_statuses(studio_name):
    """Путь до *main_app:working_statuses*"""
    return f'{settings.SERVER_URL}api/{studio_name}/working_statuses/'