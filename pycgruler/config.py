# -*- coding: utf-8 -*-

"""Константы. Импортируются в :ref:`settings-page` доступны в *cgruler.settings* """

DATA_FOLDER='.cgruler_b3d'
"""str: Имя директории хранения локальных настроек пользователя расположение в ~/ """

AUTH_FILE='auth_data.json'
"""srt: Имя файла где хранятся параметры для аутентификации пользователя. """

SETTING_FILE='setting.json'
"""str: Имя файла где храниться словарь общих настроек. Ключи словаря:

    * ``projects_folder`` - путь до :attr:`settings.PROJECTS`
"""

COOKIE_NAME = '.cookie'
"""str: файл куки. """

PATH_IDENTIFIER = 'id'
"""str: Ключ из ``task[link]`` значение которого используется для построения путей директорий. """

PROJECTS='CGRuler_Projects_Main'
"""str: Название директории где распологаются проекты. """

SERVER_URL='http://127.0.0.1:8000/'
"""str: root url """

TASK_DATA_COMPONENT='task_data'
"""str: Имя компонента содержащего архив с данными для загрузки версии. """

TASK_DATA_EXT='.zip'
"""str: Расширение архива версии. """

REVIEW_COMPONENT="review"
"""str: Имя компонента ревью, содержащего видео в формате .mp4"""

ANIMATIC_COMPONENT="animatic"
"""str: Имя компонента аниматика, содержащего видео в формате .mp4"""

TASK_TYPE_PRIORITY=("modeling", "shading", "rigging", "layout", "animation")
"""tuple: Список типов задач с возрастающим приоритетом для загрузки в качестве входящей задачи. """

FILM_OBJECT_TYPES=(
    "Episode",
    "Sequence"
)
"""tuple: Имена типов ассетов в которых ведётся работа на монтажном столе и требуется *animatic_tools* """

ANIMATION_OBJECT_TYPES=(
    "Shot",
)
"""tuple: Имена типов ассетов в которых ведётся работа над анимацией и требуется *animation_tools* """

ANIMATION_TASK_PROCESSES=(
    "layout",
    "animation",
    "vfx",
    "sound",
    "voice",
    "rendering",
    "composition",
    "color_correction",
)
"""list: Имена процессов связанные с анимацией. """

TASK_TYPE_LINKS=(
    ("modeling","shading"),
    ("shading","rigging"),
    ("rigging","layout"),
    ("layout", "animation"),
    ("animation","fx"),
    ("animation","compositing"),
    ("animation","rendering"),
    ("fx","compositing"),
    ("fx","rendering"),
    ("compositing","rendering"),
)
"""tuple: кортеж с парами типов задач, по установлению связей. 0 - тип задачи исходящей, 1 - тип задачи входящей. 

Если один тип может в разных шаблонах иметь различные выходы, то порядок следования от ближайших возможных путей к дальним.
"""

SHOTS_FOLDER="Shots"
"""str: имя Folder контейнера для размещения шотов эпизода. """

TEXTURES_FOLDER = "textures"
"""str: Название директории текстур задачи. """

FOLDERS={
    "common":("versions", ".backup", "meta"),
    "AssetModel":("versions", ".backup", TEXTURES_FOLDER, "meta")
}
"""Состав директорий в папке задачи, по типам ассетов (parent.object_type.name). """

EXCLUDED_DIRECTORIES=[".backup", "versions"]
"""list: Имена директорий, которые не загружаются в task_data.zip при ``commit``. """

VERSION_ARCHIVE_NAME='task-data.zip'
"""str: имя архива версии рабочего файла. """