# -*- coding: utf-8 -*-

"""API клиента к облакку и локальной файловой структуре. """

import os
import json
import shutil
import tempfile
import threading
import logging
import traceback
import ssl
from urllib.request import urlretrieve
from zipfile import ZipFile

logger = logging.getLogger(__name__)

try:
    import path_utils
    import settings
    import db
    import srvconn
except:
    from . import path_utils
    from . import settings
    from . import db
    from . import srvconn


def _make_task_data_zip(task, version):
    """Создание архива рабочего файла в темп директории.

    Parameters
    ----------
    task : dict
        Текущая задача.
    version : int
        Номер локальной версии. 

    Returns
    -------
    str
        Путь до созданного архива
    """
    # (0) get paths
    zip_dir = tempfile.mkdtemp()

    # (1) copy work file to zip_dir
    b,vpath = path_utils.get_version_path_of_workfile(task, version)
    if not b:
        logger.warning(vpath)
    ext=os.path.splitext(vpath)[1]
    file_name = os.path.basename(vpath).split('_v')[0]
    tpath = os.path.join(zip_dir, f"{file_name}{ext}")
    shutil.copyfile(vpath, tpath)

    # (2) copy directories to zip_dir
    b,task_dir = path_utils.get_task_folder_path(task, create=False)
    if not b:
        logger.warning(task_dir)
    if not os.environ["CGRULER_B3D_CURRENT_ASSET_TYPE"] in settings.FOLDERS:
        asset_type = "common"
    else:
        asset_type = os.environ["CGRULER_B3D_CURRENT_ASSET_TYPE"]

    for folder in settings.FOLDERS[asset_type]:
        if folder in settings.EXCLUDED_DIRECTORIES:
            continue
        from_path = os.path.join(task_dir, folder)
        to_path = os.path.join(zip_dir, folder)
        if os.path.exists(from_path):
            shutil.copytree(from_path, to_path)

    # (3) copy other files to zip_dir
    other_paths=[]
    # for f in os.listdir(task_dir):
    #     fpath=os.path.join(task_dir, f)
    #     if os.path.isfile(fpath) and __test_is_video(fpath) and f.startswith("Animatic_"):
    #         video_zip_path=os.path.join(zip_dir, f)
    #         shutil.copyfile(fpath, video_zip_path)
    #         other_paths.append(video_zip_path)
        
    #(1) make ZIP
    zip_name = settings.VERSION_ARCHIVE_NAME
    zip_path = os.path.join(zip_dir, zip_name)
    #(2)
    os.chdir(zip_dir)
    #(3)
    with ZipFile(zip_name, "w") as zf:
        for path in (tpath,*other_paths):
            if os.path.exists(path):
                zf.write(os.path.basename(path))
                # zf.write(path)
        for folder in settings.FOLDERS[asset_type]:
            if os.path.exists(folder):
                # pack_zip(folder, '')
                for root, dirs, files in os.walk(folder):
                    for file in files:
                        if os.path.samefile(root, folder):
                            f_path=os.path.join(os.path.basename(folder), file)
                        else:
                            f_path=os.path.join(os.path.basename(folder), os.path.relpath(root, start=folder), file)
                        print(f'[file]: {f_path}')
                        zf.write(f_path)
                    for fld in dirs:
                        if os.path.samefile(root, folder):
                            d_path=os.path.join(os.path.basename(folder), fld)
                        else:
                            d_path=os.path.join(os.path.basename(folder), os.path.relpath(root, start=folder), fld)
                        print(f'[folder]: {d_path}')
                        zf.write(d_path)

    return zip_path


def _push_files_to_version(task, files, description, version_type="Commit", id_version=None):
    """Загрузка файлов в облако в существующую или новую версию.

    .. note:: На каждый файл из ``files`` будет создан компонент по имени ключа этого словаря.

    Parameters
    ----------
    task : dict
        Текущая задача.
    files : dict
        Ключи - имена создаваемых компонет, значения - пути до загружаемых файлов.
    description : str
        Описание версии, не используется если передавать ``id_version``.
    version_type : str, optional
        имя типа версии, по умолчанию ``Commit``, не используется если передавать ``id_version``.
    id_version : str
        ``id`` версии в которую загружаются файлы. Если не передавать - будет создана новая версия.

    Returns
    -------
    None
    """
    logger.info("_push_files_to_version()")

    #VERSION
    if id_version:
        version=srvconn.version(id_version)
    else:
        b,v_type=srvconn.version_type(version_type)
        if not b:
            logger.warning(v_type)
            raise Exception(v_type)
        logger.info(v_type)
        data={
            "task":task["id"],
            "description": description,
            "type": v_type["id"]
            }
        b,version=srvconn.version_new(**data)
        logger.info(f"created version: {version}")

    #COMPONENTS
    for f_name, f_path in files.items():
        data=dict(
            name=f_name,
            version=version["id"],
            file=f_path
            )
        b,component=srvconn.version_component_new(**data)
        logger.info(f"created component: {component}")


def _download_file_from_url(url, file_name, download_path=False):
    """
    Скачивание файла из *url* в темп с указанным именем файла.

    Parameters
    ----------
    url : str
        Путь для скачивания.
    file_name : str
        Имя для скачиваемого файла.
    download_path : str
        Полный путь для скачивания, при этом ``file_name`` не используется.

    Returns
    -------
    tuple
        (True, download_path) или (False, comment)
    """
    if not download_path:
        download_dir = tempfile.mkdtemp()
        path=os.path.join(download_dir, file_name)
    else:
        path=download_path
    try:
        ssl._create_default_https_context = ssl._create_unverified_context
        urlretrieve(url, path)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return (False, f'Exception in _download_file_from_url({url}, {file_name}: {str(e)}')

    return(True, path)


def _download_component(url=False, version_id=False, component_name="task-data"):
    """Скачивание компонента версии задачи.
    
    Parameters
    ----------
    url : str optional
        Путь до компонента. Проверяется до проверки ``version_id``.
    version_id : str optional
        Загружаемая версия. Если не передавать - будет скачена последняя версия. Не имеет смысла если передан ``url``.
    component_name : str optional
        Имя загружаемого компонента

    Returns
    -------
    tuple
        (True, "path to download file") или (False, comment)
    """
    # (1) GET URL OF COMPONENT
    if url:
        path=url
    else:
        b,path=srvconn.get_url_of_task_data_component(version_id)
        # print(f'*** {path}')
        if not b:
            return(False,path)

    # (2) DOWNLOAD
    return _download_file_from_url(path, f'{settings.TASK_DATA_COMPONENT}{settings.TASK_DATA_EXT}')


def login(username, password):
    """Авторизация пользователя.
    
    Заполняется переменная окружения :ref:`CGRULER_B3D_AUTH_USER`
    """
    b,r= srvconn.login(username, password)
    if b:
        os.environ["CGRULER_B3D_AUTH_USER"]=username
    else:
        os.environ["CGRULER_B3D_AUTH_USER"]='No authorization'
    return (b,r)


def projects_get_active_list():
    """Список активных проектов студий в которых состоит авторизаванный пользователь."""
    b,projects=srvconn.projects_active_list()

    if not b:
        return(b,projects)

    p_dict=dict()
    p_enums=list()
    for p in projects:
        # # (convert custom attributes)
        # custom_attrs=dict()
        # for attr in p["custom_attributes"]:
        #     custom_attrs[attr["name"]]=attr[f'_{attr["value_type"].lower()}_value']
        # p["custom_attributes"]=custom_attrs
        # ()
        p_dict[p['id']]=p
        p_enums.append((p["id"], p["label"], ""))
    
    return(True, (p_enums, p_dict))


def project_set_current(project):
    """Установка проекта в качестве текущего. 

    Заполнение переменных окружения:

    * :ref:`CGRULER_B3D_CURRENT_STUDIO_ID`
    * :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_NAME`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_FULLNAME`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_FPS`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_WIDTH`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_HEIGHT`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_FSTART`
    * :ref:`CGRULER_B3D_CONTAINER_TYPES`
    * :ref:`CGRULER_B3D_STATUSES_TYPES`
    * :ref:`CGRULER_B3D_WORKING_STATUSES`


    Parameters
    ----------
    project : dict
        Словарь данных проекта.

    Returns
    -------
    None
    """
    os.environ["CGRULER_B3D_CURRENT_STUDIO_ID"]=str(project['studio']['id'])
    os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"]=project['studio']['name']
    os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]=project['id']
    os.environ["CGRULER_B3D_CURRENT_PROJECT_NAME"]=project["name"]
    os.environ["CGRULER_B3D_CURRENT_PROJECT_FULLNAME"]=project["label"]
    os.environ["CGRULER_B3D_CURRENT_PROJECT_FPS"]=str(project["custom_attributes"]["fps"])
    os.environ["CGRULER_B3D_CURRENT_PROJECT_WIDTH"]=str(project["custom_attributes"]["width"])
    os.environ["CGRULER_B3D_CURRENT_PROJECT_HEIGHT"]=str(project["custom_attributes"]["height"])
    if "fstart" in project["custom_attributes"]:
        os.environ["CGRULER_B3D_CURRENT_PROJECT_FSTART"]=str(project["custom_attributes"]["fstart"])
    else:
        os.environ["CGRULER_B3D_CURRENT_PROJECT_FSTART"]="1000"
    #(containers types)
    b,r=container_types_get_list()
    if b:
        os.environ["CGRULER_B3D_CONTAINER_TYPES"]=json.dumps(r[1])
    #(statuses types)
    b,r=statuses_types_get_list()
    if b:
        os.environ["CGRULER_B3D_STATUSES_TYPES"]=json.dumps(r[1])
    #(statuses)
    b,r=statuses_get_list()
    if b:
        os.environ["CGRULER_B3D_WORKING_STATUSES"]=json.dumps(r[1])


def task_set_current(task):
    """Устанавливает задачу в качестве текущей.

    Заполнение переменных окружения:

    * :ref:`CGRULER_B3D_CURRENT_TASK_ID`
    * :ref:`CGRULER_B3D_CURRENT_ASSET_NAME`
    * :ref:`CGRULER_B3D_CURRENT_ASSET_TYPE`
    * :ref:`CGRULER_B3D_CURRENT_ASSET_ID`
    * :ref:`CGRULER_B3D_SCENE_FEND` для задач шота.

    Parameters
    ----------
    task : dict
        task
    container_types : dict, optional
        Словарь типов контейнеров по ``id``, результат функции :func:`container_types_get_list`

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # (container_types)
    container_types_str=os.environ.get("CGRULER_B3D_CONTAINER_TYPES")
    if not container_types_str:
        b,r=container_types_get_list()
        if b:
            container_types=r[1]
        else:
            return(False, r)
    else:
        container_types=json.loads(container_types_str)
    # (other)
    os.environ["CGRULER_B3D_CURRENT_TASK_ID"]=task["id"]
    os.environ["CGRULER_B3D_CURRENT_ASSET_NAME"]=task['parent']['name']
    os.environ["CGRULER_B3D_CURRENT_ASSET_TYPE"]=container_types[str(task['parent']['type'])]['name']
    os.environ["CGRULER_B3D_CURRENT_ASSET_ID"]=task['parent']['id']
    if os.environ["CGRULER_B3D_CURRENT_ASSET_TYPE"]=="Shot":
        b,shot=container(os.environ["CGRULER_B3D_CURRENT_ASSET_ID"])
        if not b:
            self.report({'WARNING'}, shot)
            return{'FINISHED'}
        os.environ["CGRULER_B3D_SCENE_FEND"]=str(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_FSTART"]) + shot['custom_attributes']['fend'] - shot['custom_attributes']['fstart'])
    return (True, "Ok!")


def tasks_get_assigned_list(statuses=["Non Started", "Ready", "Rejected", "Working", "Paused", "Checking"]):
    """Список задач текущего проекта назначенных на авторизованного пользователя.

    ``id`` проекта читается из :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`

    Parameters
    ----------
    statuses : list optional
        Перечень имён статусов возвращаемых задач.

    Returns
    -------
    tuple
        (True, ([tasks], {tasks_dict})) или (False, comment)
    """
    b,r=srvconn.tasks_assigned_list(statuses=statuses)

    if not b:
        return(b,r)

    tasks_dict=dict()
    for t in r:
        tasks_dict[t['id']]=t
    return(True, (r, tasks_dict))


def tasks_get_checked_list(statuses=["Checking"]):
    """Список задач текущего проекта назначенных на авторизованного пользователя в качестве менеджера.

    ``id`` проекта читается из :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`

    Parameters
    ----------
    statuses : list optional
        Перечень имён статусов возвращаемых задач.

    Returns
    -------
    tuple
        (True, ([tasks], {tasks_dict})) или (False, comment)
    """
    b,r=srvconn.tasks_checked_list(statuses=statuses)

    if not b:
        return(b,r)

    tasks_dict=dict()
    for t in r:
        tasks_dict[t['id']]=t
    return(True, (r, tasks_dict))


def task_set_status(task, status):
    """
    Изменяет статус задачи

    Использует переменные окружения:

    * :ref:`CGRULER_B3D_WORKING_STATUSES`
    * :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`

    Parameters
    ----------
    task : dict
        task
    status : str
        status name

    Returns
    -------
    tuple
        (True, ok) или (False, comment)
    """
    if not os.environ.get("CGRULER_B3D_WORKING_STATUSES"):
        return (False, "No found os.environ['CGRULER_B3D_WORKING_STATUSES']")

    if status==task["status"]["name"]:
        print(f"*** no change status")
        return(True, "Ok!")
    elif status=="Working" and task["status"]["name"]!="Working":
        print(f"*** change status to Working")
        b,tasks=srvconn.containers(type__name="Task", project__id=os.environ.get("CGRULER_B3D_CURRENT_PROJECT_ID"), status__name="Working")
        if not b:
            return(b,tasks)
        print(f'[tasks]: {tasks}')
        paused_status=json.loads(os.environ["CGRULER_B3D_WORKING_STATUSES"])["Paused"]
        for t in tasks:
            srvconn.container_update(t, dict(status=paused_status["id"]))

    status=json.loads(os.environ["CGRULER_B3D_WORKING_STATUSES"])[status]
    return srvconn.container_update(task, dict(status=status["id"]))


def container_types_get_list():
    """Список и словарь по ``id`` типов контейнеров текущей студии.

    ``name`` студии читается из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`

    Returns
    -------
    tuple
        (True, (container_types_list, container_types_dict)) или (False, comment)
    """
    b,r=srvconn.container_types()
    if not b:
        return(b,r)

    types_dict=dict()
    for t in r:
        types_dict[t["id"]]=t
    # print(types_dict)

    return(True, (r,types_dict))


def statuses_types_get_list():
    """Список и словарь по ``id`` типов статусов.

    Returns
    -------
    tuple
        (True, (statuses_types_list, statuses_types_dict)) или (False, comment)
    """
    b,r=srvconn.statuses_types()
    if not b:
        return(b,r)

    types_dict=dict()
    for t in r:
        types_dict[t["id"]]=t
    # print(types_dict)

    return(True, (r,types_dict))


def statuses_get_list():
    """Список и словарь по ``name`` доступных статусов студии.

    Returns
    -------
    tuple
        (True, (statuses_list, statuses_dict)) или (False, comment)
    """
    b,r=srvconn.statuses()
    if not b:
        return(b,r)

    data_dict=dict()
    for t in r:
        data_dict[t["name"]]=t
    # print(data_dict)

    return(True, (r,data_dict))


def container(id) -> tuple:
    """Возвращает контейнер любого типа по его ``id``

    Returns
    -------
    tuple
        (True, {container_data}) или (False, comment)
    """
    b,r=srvconn.container(id)
    # if b:
    #     # (convert custom attributes)
    #     custom_attrs=dict()
    #     for attr in r["custom_attributes"]:
    #         custom_attrs[attr["name"]]=attr[f'_{attr["value_type"].lower()}_value']
    #     r["custom_attributes"]=custom_attrs

    return(b,r)


def commit(task, description, status_name, filepath, push=True):
    """Создание рабочей версии локально и фоновая выгрузка версии на Фтрек.

    Parameters
    ----------
    task : task
        Текущая задача.
    description : str
        Коментарий к версии.
    status_name : str
        Статус назначаемый задаче.
    filepath : str
        Путь к текущему файлу
    push : bool optional
        Если True - версия будет загружена в облако.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # SAVE LOCAL VERSION
    # -- определить номер новой локальной версии
    b,version = db.get_new_work_version(task)
    if not b:
        return(False, version)
    # -- path version
    b,path = path_utils.get_version_path_of_workfile(task, version)
    if not b:
        return(False, path)
    else:
        if not os.path.exists(os.path.dirname(path)):
            path_utils._create_path(os.path.dirname(path), True)
    try:
        shutil.copyfile(filepath, path)
    except Exception as e:
        return(False, f'{e}')

    # WRITE TO DB
    b,v = db.add_work_version(task, description)
    if not b:
        return(False, v)

    # CHANGE STATUS
    b,r=task_set_status(task, status_name)
    if not b:
        return(b,r)

    # PUSH
    if push:
        zip_path=_make_task_data_zip(task, version)
        logger.info(f'created version archive: {zip_path}')

        # ФОНОВАЯ ВЫГРУЗКА
        t = threading.Thread(target=_push_files_to_version, args=(task, {settings.VERSION_ARCHIVE_NAME.split(".")[0]:zip_path}, description), kwargs={})
        t.setDaemon(True)
        t.start()
    
    return(True, v)


def task_get_versions(task_id=False, version_type="Commit"):
    """Список версий загруженных в облако, определённого типа.

    Parameters
    ----------
        task_id : str optional
            ``id`` задачи
        version_type : str optional default "Commit"
            имя типа версии.

    Returns
    -------
    tuple
        (True, ([versions], {versions_dict})) или (False, comment)
    """
    if not task_id:
        task_id=os.environ.get("CGRULER_B3D_CURRENT_TASK_ID")
    kw=dict(
        task__id=task_id,
        type__name=version_type
        )
    b,versions=srvconn.versions(**kw)
    if not b:
        return(b,versions)

    versions_dict=dict()
    for v in versions:
        versions_dict[v["id"]]=v

    return(True, (versions, versions_dict))


def download_version(task, version, component_name="task-data") -> tuple:
    """Загрузка версии с ftrack и создание локальной.

    Parameters
    ----------
    task : dict
        Текущая задача.
    version : dict
        Загружаемая версия.

    Returns
    -------
    tuple
        (True, comment) или (False, comment)
    """
    # (0) exists Component
    component=None
    for c in version["components"]:
        if c["name"]==component_name:
            component=c
    if not component:
        return(False, f"Conmonent \"{component_name}\" not found!")

    # (1)
    b,r=path_utils.check_file_structure(task)
    if not b:
        return(False,r)
    
    # (2) Пути
    # -- НОМЕР НОВОЙ ЛОКАЛЬНОЙ ВЕРСИИ
    b,vers = db.get_new_work_version(task)
    if not b:
        return(False, vers)
    # -- TASK FOLDER
    b,task_folder=path_utils.get_task_folder_path(task)
    if not b:
        return(False, task_folder)
    # -- VERSIONS PATH
    b,version_path=path_utils.get_version_path_of_workfile(task, vers)
    if not b:
        return(False, version_path)
    b,top_version_path=path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False, top_version_path)
    else:
        top_version_name=os.path.basename(top_version_path)

    logger.debug(f"{vers} - {version_path}")
    
    # (3) DOWNLOAD
    url=component["file"]
    b,dowload_path=_download_component(url=url, component_name=component_name)
    if not b:
        return(False, dowload_path)

    logger.debug(f"download {vers} to {dowload_path}")
        
    # (4) EXTRACK
    # -- to task_folder
    download_dir=os.path.dirname(dowload_path)
    with ZipFile(dowload_path) as myzip:
        members=myzip.namelist()
        # print(members)
        members.remove(top_version_name)
        # print(members)
        myzip.extractall(path=task_folder, members=members)
        myzip.extractall(path=download_dir, members=[top_version_name])
    # -- to version
    from_path = os.path.join(download_dir, top_version_name)
    shutil.copyfile(from_path, version_path)

    # (5) DB
    # -- work_version
    b,r=db.add_work_version(task, f"[{version['artist_username']}]: {version['description']}")
    if not b:
        return(False, r)
    b,r = db.update_work_version(task, r, version['id'])
    if not b:
        return(False, r)
    # -- loaded_version
    b,r = db.exists_loaded_version(version)
    if not b:
        return(False, r)
    if r[0]=="missing":
        b,r=db.add_loaded_version(version)
        if not b:
            return(False, r)
    else:
        b,r=db.update_loaded_version(version)
        if not b:
            return(False, r)
    return(True, f'Download to "{vers}" version')