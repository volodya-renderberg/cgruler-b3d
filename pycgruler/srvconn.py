# -*- coding: utf-8 -*-

"""Взаимодействие с *rest_api* сервера. """

import requests
import json
import traceback
import os
import logging

try:
    import path_utils
    import settings
    import db
    import urls
except:
    from . import path_utils
    from . import settings
    from . import db
    from . import urls

logger = logging.getLogger(__name__)


def _convert_custom_attributes(ob):
    """ 
    Конвертирование ``custom_attributes`` в словарь {*custom_attr_name*: *value*, ...}

    Parameters
    -----------
    ob : dict
        Объект имеющий кастомные атрибуты в параметре ``custom_attributes``.

    Returns
    -------
    dict
        Изменённый объект
    """
    custom_attrs=dict()
    for attr in ob["custom_attributes"]:
        custom_attrs[attr["name"]]=attr[f'_{attr["value_type"].lower()}_value']
    ob["custom_attributes"]=custom_attrs

    return ob


def _write_cookie(cookie):
    '''
    Parameters
    ----------
    studio : :obj:`edit_db.studio`
        Экземпляр объетка студии или любого из его потомков.
    cookie: dict
        словарь кука
    '''
    with open(settings._get_cookie_path(), 'w') as f:
        f.write(json.dumps(cookie))


def _read_cookie():
    '''
    Parameters
    ----------
    studio : :obj:`edit_db.studio`
        Экземпляр объетка студии или любого из его потомков.
    '''
    if not os.path.exists(settings._get_cookie_path()):
        raise Exception('No Auth!')
    with open(settings._get_cookie_path(), 'r') as f:
        cookie=json.load(f)
    return cookie


def _write_auth_data(auth_data) -> None:
    """Запись параметров для аутентификации в файл :attr:`pycgruler.config.AUTH_FILE`

    Parameters
    ----------
    auth_data : dict, str
        словарь или строковое представление словаря пользователя от джанго.

    Returns
    -------
    None
    """
    try:
        with open(settings._get_auth_data_file(), 'w', encoding='utf-8') as f:
            if isinstance(auth_data, str):
                f.write(auth_data)
            if isinstance(auth_data, dict):
                f.write(json.dumps(auth_data))
    except Exception as e:
        print(f'{traceback.format_exc()}')


def _make_sess():
    """
    Создаёт сессию с куком сессии авторизованного пользователя. Чтение файла ``cookie``.

    Returns
    -------
    tuple
        cookie, sess
    """
    cookie=_read_cookie()
    sess = requests.Session()
    cj=requests.utils.cookiejar_from_dict(cookie)
    sess.cookies=cj
    return cookie, sess


def _request_get(url, params=None):
    """Запрос на сервер, возвращает json компонент ответа.

    Parameters
    ----------
    url : str
        url
    params : dict
        Параметры фильтрации в стиле `django <https://djbook.ru/rel3.0/ref/models/querysets.html#field-lookups>`_

    Returns
    -------
    tuple
        (True, request.json()) или (False, comment)
    """
    cookie, sess =_make_sess()
    if isinstance(params, dict):
        for k,v in params.items():
            if isinstance(v, list) or isinstance(v, dict) or isinstance(v, tuple):
                params[k]=json.dumps(v)
    r=sess.get(url, cookies=cookie, params=params)
    if not r.ok:
        return(False, r.text)
    return (True, r.json())


def _request_push(url, data, files=dict(), method="PATCH"):
    """Редактирование или создание модели.

    Parameters
    ----------
    url : str
        url
    data : dict
        Изменяемые значения.
    files : dict
        Передаваемые файлы
    method : str
        Вид запроса ``PATCH``, ``POST``

    Returns
    -------
    tuple
        (True, request.json()) или (False, comment)
    """
    cookie, sess =_make_sess()
    # print(f"[cookie]: {cookie}")
    
    #(GET)
    r=sess.get(url, cookies=cookie)
    if not r.ok:
        return(False, r.text)

    #(PATCH)
    csrf_token = r.cookies.get('csrftoken')
    # print(f"[csrftoken]: {csrf_token}")
    
    if method=="PATCH":
        r=sess.patch(url, data=data, files=files, headers={"X-CSRFToken":csrf_token}, cookies=cookie)
    elif method=="POST":
        r=sess.post(url, data=data, files=files, headers={"X-CSRFToken":csrf_token}, cookies=cookie)

    logger.info(r.text)
        
    if not r.ok:
        return(False, r.text)

    return (True, r.json())


def login(username, password):
    '''
    Логин, запись файлов :attr:`pycgruler.config.COOKIE_NAME` и :attr:`pycgruler.config.AUTH_FILE`.

    Parameters
    ----------
    username : str
        ...
    password : str
        ...

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    '''
    login_url=urls.api_login()
       
    sess = requests.Session()
    # (1) get to login
    try:
        r1=sess.get(login_url)
    except requests.exceptions.ConnectionError as e:
        print(e)
        return(False, "Connection refused!")

    if not r1.ok:
        return(False, r1.text)

    # (2) post to login
    csrf_token = r1.cookies.get('csrftoken')
    r2 = sess.post(login_url, data=dict(username=username, password=password, csrfmiddlewaretoken=csrf_token))

    if not r2.ok:
        return(False, r2.text)

    # (3) write cookie
    cookie=r2.cookies
    _write_cookie(dict(cookie))

    # (4) write user_data
    print(r2.text)
    _write_auth_data({"username":username,"password":password})
    # 
    return (True, "Ok!")


def projects_active_list():
    """Список активных проектов студий в которых состоит авторизаванный пользователь.

    * Обрабатывает ``custom_attributes``.

    Returns
    -------
    tuple
        (True, [projects]) или (False, comment)
    """
    url=urls.studios_list()
    cookie, sess =_make_sess()

    # (GET - studios list)
    params=dict(artists__user__username=os.environ["CGRULER_B3D_AUTH_USER"])
    r1=sess.get(url, cookies = cookie, params=params)
    if not r1.ok:
        return(False, r1.text)

    # (GET - projects list)
    prjects=list()
    for studio in r1.json():
        url=urls.projects_list(studio['name'])
        params=dict(active=True)
        r=sess.get(url, cookies = cookie, params=params)
        if not r.ok:
            return(False, r.text)
        prjects=prjects+r.json()

    #
    for ob in prjects:
        _convert_custom_attributes(ob)    

    return(True, prjects)


def tasks_assigned_list(statuses=None) -> tuple:
    """Список задач текущего проекта назначенных на авторизованного пользователя в качестве исполнителя.

    * Обрабатывает ``custom_attributes``.
    * ``id`` проекта читается из :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`
    * ``name`` студии читается из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`

    Parameters
    ----------
    statuses : list optional
        Перечень имён статусов возвращаемых задач.

    """
    url=urls.assigned_tasks(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    
    # (GET - tasks list)
    if statuses:
        b,r= _request_get(
            url,
            params=dict(
                status__name__in=statuses,
                project__id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]
                ))
    else:
        b,r= _request_get(
            url,
            params=dict(project__id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"])
            )
    #
    if b:
        for ob in r:
            _convert_custom_attributes(ob)
    return(b,r)


def tasks_checked_list(statuses=None) -> tuple:
    """Список задач текущего проекта назначенных на авторизованного пользователя в качестве менеджера.

    * Обрабатывает ``custom_attributes``.
    * ``id`` проекта читается из :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`
    * ``name`` студии читается из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`

    Parameters
    ----------
    statuses : list optional
        Перечень имён статусов возвращаемых задач.

    """
    url=urls.checked_tasks(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    
    # (GET - tasks list)
    if statuses:
        b,r= _request_get(
            url,
            params=dict(
                status__name__in=statuses,
                project__id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]
                ))
    else:
        b,r= _request_get(
                url,
                params=dict(project__id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"])
                )
    #
    if b:
        for ob in r:
            _convert_custom_attributes(ob)
    return(b,r)


def container_types() -> tuple:
    """
    Список типов контейнеров текущей студии.

    ``name`` студии читается из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`

    Returns
    -------
    tuple
        (True, container_types_list) или (False, comment)
    """
    url=urls.container_types(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    return _request_get(url)


def statuses_types() -> tuple:
    """
    Список типов статусов.

    Returns
    -------
    tuple
        (True, container_types_list) или (False, comment)
    """
    url=urls.working_status_types()
    return _request_get(url)


def statuses() -> tuple:
    """
    Список статусов текущей студии.

    Имя студии берётся из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`

    Returns
    -------
    tuple
        (True, container_types_list) или (False, comment)
    """
    url=urls.working_statuses(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    return _request_get(url)


def container(container_id) -> tuple:
    """Возвращает контейнер любого типа по его ``id``

    * Обрабатывает ``custom_attributes``.
    * ``name`` студии читается из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`
    
    Returns
    -------
    tuple
        (True, {container_data}) или (False, comment)
    """
    url=urls.container(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"], container_id)
    b,r= _request_get(url)
    if b:
        r=_convert_custom_attributes(r)
    return(b,r)


def containers(**kwargs) -> tuple:
    """Возвращает контейнеры любого типа текущей студии по параметрам фильтрации. 

    Если не передавать параметры фильтрации, вернёт все.

    * Обрабатывает ``custom_attributes``.
    * ``name`` студии читается из :ref:`CGRULER_B3D_CURRENT_STUDIO_NAME`

    Parameters
    ----------
    kwargs : dict
        Именованные аргументы - `Параметры фильтрации в стиле django <https://djbook.ru/rel3.0/ref/models/querysets.html#field-lookups>`_
        Все аргументы объединяются ``AND``.
    
    Returns
    -------
    tuple
        (True, [containers]) или (False, comment)
    """
    url=urls.containers(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    b,r= _request_get(url, params=kwargs)
    if b:
        for ob in r:
            _convert_custom_attributes(ob)
    return(b,r)


def container_update(container, params):
    """редактирование контейнера.

    Parameters
    ----------
    container : dict
        Контейнер
    params : dict
        Изменяемые значения.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    url=urls.container_update(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"], container["id"])
    return _request_push(url, params)


def versions(**kwargs):
    """Список версий загруженных в облако, определённого типа.

    Parameters
    ----------
    kwargs : dict
        Именованные аргументы - `Параметры фильтрации в стиле django <https://djbook.ru/rel3.0/ref/models/querysets.html#field-lookups>`_
        Все аргументы объединяются ``AND``.

    Returns
    -------
    tuple
        (True, [versions]) или (False, comment)
    """
    url=urls.versions(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"], **kwargs)
    return _request_get(url)


def version(id):
    """Возвращает версию по ``id`` 

    Parameters
    ----------
    id : str
        ``id`` версии
    """
    url=urls.version(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"], id)
    return _request_get(url)


def version_new(**data) -> tuple:
    """Создаёт версию задачи.

    Parameters
    ----------
    data : dict

            {
                task: task_id (str),

                description: text (str),

                type: version_type_id (int)
            }

    Returns
    -------
    tuple
        (True, request.json()) или (False, comment)
    """
    url=urls.version_new(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    logger.info(f"data: {data}")
    return _request_push(url, data, method="POST")


def version_component_new(**data):
    """
    Создаёт файловый компонент версии.

    Parameters
    ----------
    data : dict

            {
                name: name (str),

                version: version_id (str),

                file: path to file (str)
            }

    Returns
    -------
    tuple
        (True, request.json()) или (False, comment)
    """
    url=urls.version_component_new(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"])
    files=dict()
    if "file" in data and os.path.exists(data["file"]):
        files={'file': open(data["file"], 'rb')}
        data.pop("file")
    logger.info(f"version_component_new - data: {data}, files: {files}")
    return _request_push(url, data, files=files, method="POST")


def version_type(name: str):
    """Возвращает тип версии данной студии по имени. """
    kw=dict(name=name)
    url=urls.version_types(os.environ["CGRULER_B3D_CURRENT_STUDIO_NAME"], **kw)
    b,r= _request_get(url)
    if b:
        return(b, r[0])
    else:
        return(b,r)