"""Взаимодействия с локальными таблицами базы данных.

* В директории проекта создаётся *sqlite* таблица для записи версий загруженных с CGRuler.
* В директории каждой задачи создаётся *sqlite* таблица учёта локальных рабочих версий.
"""

import sqlite3
import os
import datetime

try:
    import path_utils
except:
    from . import path_utils

BACKUP_TABLE={
"Task":[
    '',
],
"Folder":[
    '',
]}
"""Таблицы базы данных бекап данных. Ключ - имя таблицы, значение - список команд создания. """

TABLES={
"Version":[
    'local_version INTEGER PRIMARY KEY AUTOINCREMENT',
    'created TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
    'version_id TEXT',
    'description TEXT',
],
"LoadedVersion":[
    'task_id TEXT PRIMARY KEY',
    'version_id TEXT NOT NULL',
    'created TIMESTAMP NOT NULL',
    'loaded TIMESTAMP DEFAULT CURRENT_TIMESTAMP',
]}
"""Таблицы базы данных. Ключ - имя таблицы, значение - список команд создания. """

def _db_set(path, com, data_com=False) -> tuple:
    """Внесение изменений в базу данных. 

    Parameters
    ----------
    path : str
        Путь до файла базы данных Sqlite3.
    com : str
        Строка команды
    data_com : tuple
        Кортеж значений подставляемых в команду.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """  
    try:
        # -- CONNECT  .db
        conn = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        if data_com:
            c.execute(com, data_com)
        elif com:
            c.execute(com)
        else:
            pass
    except Exception as e:
        try:
            conn.close()
        except:
            pass
        print('#'*3, 'Exception in _db_set()', e)
        print('#'*3, 'com:', com)
        print('#'*3, 'data_com:', data_com)
        return(False, 'Exception in _db_set(), please look the terminal!')
    
    conn.commit()
    conn.close()
    return(True, 'Ok!')

def _db_get(path, com, data_com=False) -> tuple:
    """Чтение строк из базы данных. 

    Parameters
    ----------
    path : str
        Путь до файла базы данных Sqlite3.
    com : str
        Строка команды
    data_com : tuple
        Кортеж значений подставляемых в команду.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    try:
        # -- CONNECT  .db
        conn = sqlite3.connect(path, detect_types=sqlite3.PARSE_DECLTYPES|sqlite3.PARSE_COLNAMES)
        conn.row_factory = sqlite3.Row
        c = conn.cursor()
        if data_com:
            c.execute(com, data_com)
        else:
            c.execute(com)
        data=c.fetchall()
        conn.close()
        return(True, data)
        
    except Exception as e:
        try:
            conn.close()
        except:
            pass
        print('#'*3, 'Exception in _db_get()', e)
        print('#'*3, 'com:', com)
        print('#'*3, 'data_com:', data_com)
        return(False, 'Exception in _db_get(), please look the terminal!')

def _db_create_table(path, table) -> tuple:
    """Создание таблицы sqlite3 при отсутствии.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    com = f'CREATE TABLE IF NOT EXISTS {table} ({", ".join(TABLES[table])})'
    return _db_set(path, com)

def _insert(path, table, **kw) -> tuple:
    """Вставка строки в таблицу.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    kw : dict
        вставляемая строка, ключи - имена столбцов.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    keys=[]
    val=[]
    values=[]

    for item in kw.items():
        keys.append(item[0])
        val.append("?")
        values.append(item[1])

    com = f'INSERT INTO {table} ({", ".join(keys)}) VALUES ({", ".join(val)})'
    return _db_set(path, com, data_com=values)

def _delete(path, table, where) -> tuple:
    """Удаление строки из таблицы.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    where : dict
        Условия поиска удаляемой строки. Каждая пара ключ-значение объединяются условием ``AND``.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    where_data=[]
    com_values=[]

    for key in where:
        where_data.append(f'{key} = ?')
        com_values.append(where[key])

    com = f'DELETE FROM {table} WHERE {" AND ".join(where_data)}'
    return _db_set(path, com, data_com=com_values)

def _update(path, table, where, values) -> tuple:
    """Редактирование строки таблицы.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    where : dict
        Условия поиска изменяемой строки. Каждая пара ключ-значение объединяются условием ``AND``.
    values : dict
        Изменяемые данные.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    set_data=[]
    where_data=[]
    com_values=[]

    for key in values:
        set_data.append(f'{key} = ?')
        com_values.append(values[key])
    for key in where:
        where_data.append(f'{key} = ?')
        com_values.append(where[key])

    com = f'UPDATE {table} SET {", ".join(set_data)} WHERE {" AND ".join(where_data)}'
    return _db_set(path, com, data_com=com_values)

def _get(path, table, where=False, sort=False, limit=False) -> tuple:
    """Поиск и чтение строк таблицы.

    Parameters
    ----------
    path : str
        Путь до файла базы данных sqlite.
    table : str
        Название таблицы, ключ из :attr:`TABLES`.
    where : dict optional
        Условия поиска изменяемой строки. Каждая пара ключ-значение объединяются условием ``AND``.
    sort : list, tuple optional
        список имён столбцов для сортировки, если перед именем столбца поставить ``-`` то будет обратная сортировка.
    limit : int optional
        количество отдаваемых строк.

    Returns
    -------
    tuple
        (True, string) или (False, comment)

    """
    # test table
    b,r=_db_create_table(path, table)
    if not b:
        return(b,r)

    com=f'SELECT * FROM {table}'
    com_data=[]

    if where:
        where_string=[]
        for key in where:
            where_string.append(f'{key} = ?')
            com_data.append(where[key])

        com=f'{com} WHERE {", ".join(where_string)}'

    if sort:
        sorting=[]
        for item in sort:
            if item.startswith('-'):
                sorting.append(f'{item[1:]} DESC')
            else:
                sorting.append(item)
        com = f'{com} ORDER BY {", ".join(sorting)}'

    if limit:
        com = f'{com} LIMIT {limit}'
    
    # print(com)
    # print(com_data)

    return _db_get(path, com, com_data)

def add_work_version(task, description: str) -> tuple:
    """Создание записи о новой work версии.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.

    Returns
    -------
    tuple
        (True, num_of_new_version) или (False, comment)
    """
    if not description:
        return(False, "description is a required parameter")

    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # insert
    b,r = _insert(path, table, description=description)
    if not b:
        return (b, r)

    # get version
    b,r = _get(path, table, sort=['-local_version'], limit=1)
    if not b:
        return (b, r)
    else:
        return (True, r[0]['local_version'])

def del_work_version(task, version: int) -> tuple:
    """Удаление записи о work версии по номеру.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    version : int
        Локальный номер удаляемой версии.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    return _delete(path, table, {"local_version": version})

def update_work_version(task, version: int, version_id: str) -> tuple:
    """Заполнение поля ``version_id``, после удачной выгрузки на ftrack.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    version : int
        Локальный номер редактируемой версии.
    version_id : str
        ``id`` версии загруженной на ftrack.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    return _update(path, table, {"local_version": version}, {"version_id": version_id})

def get_work_versioin(task, version: int) -> tuple:
    """Получение строки work версии по её номеру.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    version : int
        Номер локальной версии.
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # get version
    b,r = _get(path, table, where={'local_version': version}, limit=1)
    if not b:
        return (b, r)
    else:
        return (True, r[0])

def get_all_work_versions(task) -> tuple:
    """Получение строк всех work версий.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # get all versions
    return _get(path, table, sort=['local_version'])

def get_latest_work_version(task) -> tuple:
    """Получение строки последней work версии.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    """
    b, path = path_utils.get_versions_db_path(task)
    if not b:
        return (b, path)
    table="Version"

    # get version
    b,r = _get(path, table, sort=['-local_version'], limit=1)
    if not b:
        return (b, r)
    else:
        if r:
            return (True, r[0])
        else:
            return(True, None)

def get_new_work_version(task) -> tuple:
    """
    Возвращает номер для новой версии.

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    """
    # -- определить номер последней локальной версии
    b,r = get_latest_work_version(task)
    if not b:
        return(False, r)
    if not r:
        version=1
    else:
        version=r["local_version"]+1
    return(True, version)

def add_loaded_version(version) -> tuple:
    """Создание записи о новой загруженной версии.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_project_db_path(version["task"])
    if not b:
        return (b, path)
    table="LoadedVersion"

    # insert
    return _insert(path, table, task_id=version['task']['id'], version_id=version['id'], created=datetime.datetime.fromisoformat(version['created']))
    # return _insert(path, table, task_id=version['task']['id'], version_id=version['id'], created=version['created'].format())

def update_loaded_version(version) -> tuple:
    """Обновление записи о загруженной версии (id, created, loaded).

    .. note:: Проверяет на наличие записи, при отсутствии выполняет :func:`add_loaded_version`

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    # (1) TEST EXISTS
    b,r = exists_loaded_version(version)
    if not b:
        return(False, r)
    if r[0]=="missing":
        return add_loaded_version(version)
    
    # (2) UPDATE
    b, path = path_utils.get_project_db_path(version["task"])
    if not b:
        return (b, path)
    table="LoadedVersion"

    values={
        "version_id": version['id'],
        "created": datetime.datetime.fromisoformat(version['created']),
        # "created": version['created'].format(),
        "loaded": datetime.datetime.now()
        }
    return _update(path, table, {"task_id": version['task']['id']}, values)

def del_loaded_version(version) -> tuple:
    """Удаление записи о загруженной версии.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b, path = path_utils.get_project_db_path(version["task"])
    if not b:
        return (b, path)
    table="LoadedVersion"

    return _delete(path, table, {"task_id": version['task']['id']})

def exists_loaded_version(version) -> tuple:
    """Проверка наличия или свежести загруженной версии.

    Parameters
    ----------
    version : dict
        объект версии

    Returns
    -------
    tuple
        (True, answer) или (False, comment)
        где answer(tuple) - имеет три варианта: ("missing",) ("latest",) ("old", (id, created))
    """
    b, path = path_utils.get_project_db_path(version["task"])
    if not b:
        return (b, path)
    table="LoadedVersion"

    # get
    b,r = _get(path, table, where={"task_id": version['task']['id']}, limit=1)
    if not b:
        return (b, r)

    if not r:
        return (True, ("missing",))
    elif r[0]['version_id'] == version['id']:
        return (True, ("latest",))
    else:
        return (True, ("old", (r[0]['version_id'], r[0]['created'])))

def get_latest_loaded_version(task) -> tuple:
    """Получение строки последней загруженной версии. 

    Parameters
    ----------
    task : ftrack.task
        Динамический объект задачи.
    """
    b, path = path_utils.get_project_db_path(task)
    if not b:
        return (b, path)
    table="LoadedVersion"

    # get version
    b,r = _get(path, table, where={"task_id": task['id']}, sort=['-loaded'], limit=1)
    if not b:
        return (b, r)
    else:
        if r:
            return (True, r[0])
        else:
            return(True, None)