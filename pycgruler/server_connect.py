# -*- coding: utf-8 -*-

"""Взаимодействие с сервером. Загрузка-выгрузка компонент. Отмираемый.

"""

import os
import json
import logging
import time
# import threading

import bpy

import ftrack_api as ft

try:
    import settings
    import db
except:
    from . import settings
    from . import db

# # logging
# logging.basicConfig()
# plugin_logger = logging.getLogger('com.example.example-plugin')
# plugin_logger.setLevel(logging.DEBUG)

TASK_DATA_COMPONENT='task_data'
"""str: Имя компонента содержащего архив с данными для загрузки версии. """

REVIEW_COMPONENT="ftrackreview-mp4"
"""str: Имя компонента ревью, содержащего видео в формате .mp4"""

TASK_DATA_EXT='.zip'
"""str: Расширение архива версии. """

TASK_TYPE_PRIORITY=("modeling", "shading", "rigging", "layout", "animation")
"""tuple: Список типов задач с возрастающим приоритетом для загрузки в качестве входящей задачи. """

TASK_TYPE_LINKS=(
    ("modeling","shading"),
    ("shading","rigging"),
    ("rigging","layout"),
    ("layout", "animation"),
    ("animation","fx"),
    ("animation","compositing"),
    ("animation","rendering"),
    ("fx","compositing"),
    ("fx","rendering"),
    ("compositing","rendering"),
)
"""tuple: кортеж с парами типов задач, по установлению связей. 0 - тип задачи исходящей, 1 - тип задачи входящей. 

Если один тип может в разных шаблонах иметь различные выходы, то порядок следования от ближайших возможных путей к дальним.
"""

CHEKING_STATUSES=("Pending Review", "Needs attention", "Proposed final")
"""tuple: Список имён статусов, соответствующих состоянию на проверке. """

SHOTS_FOLDER="Shots"
"""str: имя папки для размещения шотов. """


def _make_tasks_link(tasks, sess):
    """Создание взаимосвязей между задачами на основе :attr:`TASK_TYPE_LINKS`

    Parameters
    ----------
    tasks : dict
        Словарь задач, где ключ - имя типа задачи, а значение объект задачи.

    Returns
    -------
    None
    """
    maked=list()
    for out_type, in_type in TASK_TYPE_LINKS:
        if out_type in tasks and not out_type in maked:
            for t in tasks.keys():
                if t==in_type:
                    sess.create('TypedContextLink', {
                        'from': tasks[out_type],
                        'to': tasks[in_type]
                    })
                    maked.append(out_type)
    sess.commit()


def _freshness_downloaded_version(task_id, sess, startswith=False):
    """
    Проверка свежести загруженной версии задачи.

    Parameters
    ----------
    task_id : str
        id задачи.
    sess : ftrack.session
        сессия ftrack
    startswith : str, optional
        Префикс имени ассета.

    Returns
    -------
    str
        **status** - значение из ['missing', 'old', 'latest']
    """
    b, vers = get_latest_version_of_task(task_id=task_id, sess=sess, close_session=False, f=True, startswith=startswith)
    status="missing"
    if b:
        b,r=db.exists_loaded_version(vers)
        if b:
            status=r[0]
    return status


def get_session() -> tuple:
    """Создаёт и возвращает сессию, или сообщение об ошибке.


    Returns
    -------
    tuple
        (True, session) или (False, comment)
    """
    try:
        sess=ft.Session()
        return(True, sess)
    except Exception as e:
        return(False, str(e))


def closing_session(sess):
    if not sess.closed:
        sess.close()


def auth(url: str, user: str, key: str) -> tuple:
    """Аутентификация.

    Returns
    -------
    tuple
        (True, sess) или (False, comment)
    """
    url=clean_final_slash(url)
    try:
        sess=ft.Session(
            server_url=url,
            api_key=key,
            api_user=user
        )
    except Exception as e:
        print('*** Ftrack authorization error ***')
        print(e)
        return(False, 'Auth problem! Look the terminal.')
    settings.write_auth_data(url, user, key)
    # vnvs
    os.environ["CGRULER_SERVER"]=url
    os.environ["CGRULER_API_USER"]=user
    os.environ["CGRULER_API_KEY"]=key
    #
    return (True, sess)

def get_active_projects_list(sess=False, close_session=True) -> tuple:
    """Получение списка активных проектов.

    .. note:: Читаются поля: ``name``, ``full_name``, ``id``, ``custom_attributes``

    Returns
    -------
    tuple
        (True, (p_enums, p_dict)) или (False, comment)
    """
    if not sess or sess.closed:
        sess=ft.Session()
    try:
        projects=sess.query("Project where status is 'active'")
        """
        projects=sess.query("select name, full_name, id from Project "
            "where status is 'active'")
        """
        # sess.populate(projects, 'name, full_name, id, custom_attributes')
        # sess.close()
    except Exception as e:
        print('***')
        print(e)
        if not sess.closed:
            sess.close()
        return(False, 'The problem of getting a list of projects! Look the terminal.')

    p_dict=dict()
    p_enums=list()
    with sess.auto_populating(False):
        for p in projects:
            p_dict[p['id']]=p
            p_enums.append((p["id"], p["full_name"], ""))
    if close_session and not sess.closed:
        sess.close()
    return(True, (p_enums, p_dict))


def get_asset_by_name(name, type_name, sess=False, close_session=True, f=False) -> tuple:
    """Возвращает ассет по имени. ``id проекта`` из :ref:`CGRULER_B3D_CURRENT_PROJECT_ID`

    Parameters
    ----------
    name : str
        имя ассета
    type_name : str
        object_type.name ("Shot", "Episode", итд.)
    sess : ftrack.session optional
        Session
    close_session : bool optional
        True - значит будет закрыта по завершению.
    f : bool optional
        True - сессия не будет закрываться при вылете.

    Returns
    -------
    tuple
        (True, asset или None-если не найден) или (False, comment-если ошибка)
    """
    if not sess or sess.closed:
        b, sess=get_session()
        if not b:
            return(False, sess)

    #(get asset)
    try:
        project_id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]
        asset=sess.query(f"{type_name} where name = '{name}' and project_id = '{project_id}'")
    except Exception as e:
        if not f:
            closing_session(sess)
        return(False, f"exception#1 in get_asset_by_name(): \n{str(e)}")
    if asset:
        asset=asset[0]
    else:
        asset=None

    #(fin)
    if close_session:
        closing_session(sess)
    return(True, asset)


def create_shot(name, template, data, sess=False, close_session=True):
    """
    Создание шота в папке Shots - на одном уровне с эпизодом.

    Parameters
    ----------
    name : str
        имя шота
    template : str
        имя темплейта для создания задач шота.
    data : dict
        параметры дополнительных атрибутов шота.

    Returns
    -------
    tuple
        (True, shot) или (False, comment)
    """
    if not sess or sess.closed:
        b, sess=get_session()
        if not b:
            return(False, sess)

    project_id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]
    episode_id=os.environ["CGRULER_B3D_CURRENT_ASSET_ID"]

    #(Shots фолдер)
    try:
        project=sess.get("Project", project_id)
        episode=sess.get("Episode", episode_id)
        episode_root_id=episode["parent"]["id"]
        episode_root=sess.get("Folder", episode_root_id)
        shots=sess.query(f"Folder where name='{SHOTS_FOLDER}' and project_id='{project_id}' and parent.id='{episode_root_id}'")
        if shots:
            shots=shots[0]
        else:
            shots=sess.create("Folder", {"name":SHOTS_FOLDER, "project": project, "parent": episode_root})
            sess.commit()
    except Exception as e:
        closing_session(sess)
        return(False, f"exception in #(Shots фолдер) in create_shot(): \n{str(e)}")
    
    #(Shot)
    try:
        tasks=dict()
        template=sess.query(f"TaskTemplate where name='{template}'")[0]
        shot=sess.create("Shot", {"name":name, "project":project, "parent":shots})
        # sess.commit()
        shot["custom_attributes"]["fstart"]= data["fstart"]
        shot["custom_attributes"]["fend"]= data["fend"]
        #(tasks)
        for item in template["items"]:
            task=sess.create("Task", {"name":item["task_type"]["name"], "type":item["task_type"], "parent":shot})
            tasks[item["task_type"]["name"]]=task
        sess.commit()
        _make_tasks_link(tasks, sess)
    except Exception as e:
        closing_session(sess)
        return(False, f"exception in #(Shot) in create_shot(): \n{str(e)}")

    #(fin)
    if close_session:
        closing_session(sess)
    return(True, shot)



def get_tasks_list(sess=False, close_session=True, check_list=False) -> tuple:
    """Получение списка задач проекта для данного пользователя.

    .. note:: Читаются поля: :attr:`construct_entity_type.TASKS_READABLE_FIELDS`

    Parameters
    ----------
    sess : ftrack_api.Session optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если *True*, то сессия будет закрыта по завершению запроса.
    check_list : bool optional
        Если *True* то  будет возвращён список задач для проверки, где данный юзер в качестве менеджера.

    Returns
    -------
    tuple
        (True, ([tasks], {tasks_dict}) или (False, comment)
    """
    project_id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]

    if not sess or sess.closed:
        sess=ft.Session()
    try:
        if check_list:
            tasks= sess.query(f'Task where (parent.parent.managers any(user.username="{sess.api_user}") '
                f'or parent.managers any(user.username="{sess.api_user}")) '
                f'and project_id = {project_id} and status.name in {CHEKING_STATUSES}')
        else:
            tasks=sess.query(f'Task where assignments any (resource.username = "{sess.api_user}") '
                f'and project_id = {project_id} and status.state.name is_not "Done"')
    except Exception as e:
        print('***')
        print(e)
        if not sess.closed:
            sess.close()
        return(False, 'The problem of getting a list of tasks! Look the terminal.')

    tasks_dict = {}
    with sess.auto_populating(False):
        for task in tasks:
            tasks_dict[task['id']]=task
    if close_session and not sess.closed:
        sess.close()
    return(True, (tasks, tasks_dict))

def get_tasks_by_id_list():
    """Возвращает список задач по списку id задач.

    Empty
    """
    pass

def get_task_by_id(task_id: str, sess=False, close_session=True) -> tuple:
    """Возвращает задачу по id.

    Returns
    -------
    tuple
        (bool, <task or "error mesage">, sess)
    """
    #(sess)
    if not sess or sess.closed:
        sess=ft.Session()
    #(get task)
    task = sess.get("Task", task_id)
    #(sess close)
    if not sess.closed and close_session:
        sess.close()
    #(end)
    return(True, task, sess)

def get_versions_list(task_id=False, sess=False, close_session=True, f=False, startswith=False):
    """Получение списка версий задачи.

    .. note:: Читаются поля: :attr:`construct_entity_type.VERSIONS_READABLE_FIELDS`

    Parameters
    ----------
    task_id : str optional
        ``id`` задачи, если не передавать - будет взято из :ref:`CGRULER_B3D_CURRENT_TASK_ID`.
    sess : ftrack_api.Session optional
        Сессия.
    close_session : bool optional
        Если **True** то сессия будет закрыта по завершению операции.
    f : bool
        Если True сессия не будет закрываться в случае неудачи.
    startswith : str, optional
        фильтр по началу названия ассета версии.

    Returns
    -------
    tuple
        (True, ([versions], {versions_dict})) или (False, comment)
    """
    if not task_id:
        task_id = os.environ["CGRULER_B3D_CURRENT_TASK_ID"]
    if not sess or sess.closed:
        sess=ft.Session()
    try:
        if startswith:
            search_block=f"and asset.name like \"{startswith}%\""
        else:
            search_block=''
        versions=sess.query(f"AssetVersion where task_id = {task_id} {search_block} order by date")
    except Exception as e:
        print('***')
        print(e)
        if not f and close_session:
            closing_session(sess)
        return(False, 'The problem of getting a list of versions! Look the terminal.')
    #
    versions_dict = {}
    with sess.auto_populating(False):
        for v in versions:
            versions_dict[v['id']]=v
    if close_session:
        closing_session(sess)
    return(True, (versions, versions_dict))

def get_latest_version_of_task(task_id=False, sess=False, close_session=True, f=False, startswith=False) -> tuple:
    """Получение последней версии задачи. 

    .. note:: Читаются поля: :attr:`construct_entity_type.VERSIONS_READABLE_FIELDS`

    Parameters
    ----------
    task_id : str optional
        ID задачи, если не передавать будет взят из :ref:`CGRULER_B3D_CURRENT_TASK_ID`
    sess : ftrack.Session optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    f : bool
        Если True сессия не будет закрываться в случае неудачи.
    startswith : str, optional
        фильтр по началу названия ассета версии.

    Returns
    -------
    tuple
        (True, version) или (False, comment)
    """
    if not task_id:
        task_id = os.environ["CGRULER_B3D_CURRENT_TASK_ID"]
    if not sess or sess.closed:
        sess=ft.Session()

    try:
        if startswith:
            search_block=f"and asset.name like \"{startswith}%\""
        else:
            search_block=''
        versions=sess.query(f"AssetVersion where task_id = {task_id} {search_block} order by date desc limit 1")
        if close_session:
            sess.close()
    except Exception as e:
        print('***')
        print(e)
        if not f:
            if not f and close_session:
                sess.close()
            return(False, f'The problem of getting a latest version of task {task_id}! Look the terminal.')

    if not versions:
        return(False, f"No versions found for task id = {task_id}")
    else:
        return(True, versions[0])


def get_reviews_list_of_asset(asset_id, last=False, startswith=False, sess=False, close_session=True, f=False, after=False) -> tuple:
    """Возвращает список версий Review ассета, или последнюю версию если Last=``True``

    Parameters
    ----------
    asset_id : str
        ID ассета
    last : bool, optional
        Если True - вернётся последняя версия, иначе - список версий, в порядке убывания.
    startswith : str, optional
        фильтр по началу названия ассета версии. Если не передавать - "Review\_"
    sess : ftrack.Session optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    f : bool
        Если True сессия не будет закрываться в случае неудачи.
    after : str
        Дата(iso) от которой идёт поиск

    Returns
    -------
    tuple
        (True, version или [versions]) или (False, comment)
    """
    if not sess or sess.closed:
        sess=ft.Session()
    if last:
        search_block=" limit 1"
    else:
        search_block=''
    try:
        if not startswith:
            startswith="Review_"
        if after:
            versions=sess.query(f"AssetVersion where task.parent.id = {asset_id} and asset.name like \"{startswith}%\" and date>\"{after}\" order by date desc{search_block}")
        else:
            versions=sess.query(f"AssetVersion where task.parent.id = {asset_id} and asset.name like \"{startswith}%\" order by date desc{search_block}")
    except Exception as e:
        print('***')
        print(e)
        if not f:
            if not f and close_session:
                sess.close()
            return(False, f'Problem in get_reviews_list_of_asset({asset_id})! Look the terminal.')

    #(end)
    if close_session:
        closing_session(sess)

    if last:
        if versions:
            return(True, versions[0])
        else:
            return(False, f"No found {startswith} of asset \"{asset_id}\"")
    else:
        return(True, versions)


def get_status_name_of_last_review(asset_id, sess=False, close_session=True, f=False):
    """
    Возвращает имя статуса задачи последнего ревью шота.

    Returns
    -------
    str
        Имя статуса задачи последнего ревью шота.
    """
    if not sess or sess.closed:
        sess=ft.Session()

    try:
        startswith="Review_"
        versions=sess.query(f"select task.status.name from AssetVersion where task.parent.id = {asset_id} and asset.name like \"{startswith}%\" order by date desc limit 1")
        if not versions:
            raise Exception(f"No versions found for asset_id = {asset_id}")
        else:
            status_name=versions[0]['task']['status']['name']
    except Exception as e:
        if not f and close_session:
            closing_session(sess)
        return(False, f'Problem in get_status_name_of_last_review({asset_id}): {e}')

    #(END)
    if close_session:
        closing_session(sess)
    return(True, status_name)


# def lag_test_of_version(task, sess=False, close_session=True, f=False) -> tuple:
#     """
#     Тест отставания локальной версии (выбор последней по дате между созданной и загруженной) от последней версии на сервере.

#     Parameters
#     ----------
#     task : task
#         Объект задачи.
#     sess : ftrack.Session optional
#         Сессия ftrack, если не передавать будет создана.
#     close_session : bool
#         Если True, то сессия будет закрыта по завершению запроса.
#     f : bool
#         Если True сессия не будет закрываться в случае неудачи.

#     Returns
#     -------
#     tuple
#         (True, num: int - количество опережающих версий, 0 - значит версия свежая)  или (False, comment)
#     """
#     if not sess or sess.closed:
#         sess=ft.Session()

#     b1,r1=get_versions_list(task_id=task["id"], sess=sess, close_session=close_session)
#     b2,r2=db.get_latest_work_version(task)
#     b3,r3=db.get_latest_loaded_version(task)

#     if not all((b1,b2,b3)):
#         if not f:
#             closing_session(sess)
#         return(False, (r1,r2,r3))

#     r1=list(r1[0])
#     if not r1:
#         lag=0
#     if not any((r2, r3)):
#         lag=len(r1)
#     else:
#         if not r2 and r3:
#             version_id=dict(r3)["version_id"]
#         elif not r3 and r2:
#             version_id=dict(r2)["version_id"]
#         else:
#             d2=dict(r2)
#             d3=dict(r3)
#             if d2["version_id"] != d3["version_id"]:
#                 if d2["created"]>d3["created"]:
#                     version_id=d2["version_id"]
#                 else:
#                     version_id=d3["version_id"]
#             else:
#                 version_id=d2["version_id"]

#         r1.reverse()

#         lag=0
#         for item in r1:
#             if item['id']==version_id:
#                 break
#             else:
#                 lag+=1

#     closing_session(sess)

#     return(True, lag)


def get_incoming_tasks(task, this_parent=True, sess=False, close_session=True, f=False) -> tuple:
    """
    Возвращает словарь по входящим задачам, для локального поиска загруженных данных из входящих задач.
    
    * ключи - ``id`` задач; 
    * значения - словари: {``task``: входящая задача, ``status``: свежесть загруженной версии}.

    .. note:: Пока что отработано только для входящих связей на прямые задачи, по связям из ассетов не будет делаться сбор.

    Parameters
    ----------
    task : ftrack.Task
        Объект задачи
    this_parent : bool, optional
        Если True - будут рассматриваться только задачи данного ассета.
    sess : ftrack.Session optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    f : bool
        Если True сессия не будет закрываться в случае неудачи.

    Returns
    -------
    tuple
        (True, **incoming_data**) или (False, conmment) где **incoming_data** - {task_id: {"task": входящая задача, "status": свежесть загруженной версии}, ...}
    """
    if not sess or sess.closed:
        sess=ft.Session()

    incoming_data=dict()
    
    # #(get Task)
    # if not isinstance(task, sess.types["Task"]):
    #     return(False, f"in server_connect.get_incoming_tasks(): Wrong type of \"task\"")

    #(get Data)
    try:
        if this_parent:
            query=f" and parent.id = \"{task['parent']['id']}\""
        incoming_tasks=sess.query(f"Task where outgoing_links any (to.id = {task['id']}){query}")
    except Exception as e:
        if not f:
            if close_session:
                closing_session(sess)
        return(False, f"Exception in get_incoming_tasks() #2: {e}")

    #(make output data)
    for in_task in incoming_tasks:
        status = _freshness_downloaded_version(in_task['id'], sess, startswith="Commit_")
        incoming_data[in_task["id"]]={"task":in_task, "status":status}

    #(end)
    if close_session:
        closing_session(sess)
    return(True, incoming_data)   


def get_downloadable_incoming_links(task_id=False, sess=False, close_session=True, for_download=False) -> tuple:
    """Возвращает список данных компонент последних версий для всех входящих задач (рекурсивно) и общий объём загружаеммых файлов.

    Parameters
    ----------
    for_download : bool
        **True** означает, что предполагается загрузка без отображения списка на загрузку в ГУИ. поэтому будут загружаться так же и входящие локаций.

    Returns
    -------
    tuple
        (True, (**components**, byte_all))  or (False, comment) где **components** - список кртежей: (Task, AsseVersion, byte, url, status)
    """
    timing = time.time()
    def _parent_is_location(task):
        """True - если родитель задачи локация. """
        return task["parent"]["object_type"]["name"]=="Asset Model" and task['parent']['type']['name'] in ("Environment", "Location")
    def _parent_is_shot(task):
        """True - если родитель задачи Shot. """
        return task["parent"]["object_type"]["name"]=="Shot"
    def _get_downloadable_incoming_links(incoming_links, tasks=[]):
        """Возвращает список id всех входящих задач (рекурсивно) """
        for link in incoming_links:
            link_from=link["from"]
            if link_from.get('object_type'):
                object_type = link_from["object_type"]["name"]
            else:
                continue
            if object_type=="Task":
                # print(f"{link_from['parent']['name']} - {link_from['name']}")
                # tasks.append((link_from['parent']['name'], link_from['name'], link_from['id']))
                tasks.append(link_from)
            elif object_type=="Asset Model":
                # -- TASK OF ASSET
                priority=0
                in_task=None
                for task in link_from['children']:
                    if task['type']['name'] in TASK_TYPE_PRIORITY:
                        index= TASK_TYPE_PRIORITY.index(task['type']['name'])
                        if index >= priority:
                            in_task=task
                            priority=index
                # print(f"{in_task['parent']['name']} - {in_task['name']}")
                if in_task:
                    # tasks.append((in_task['parent']['name'], in_task['name'], in_task['id']))
                    tasks.append(in_task)
                # -- INCOMING TASKS OF LOCATION
                if for_download:
                    if link_from["type"]["name"] in ["Environment", 'Location']:
                        _get_downloadable_incoming_links(link_from['incoming_links'], tasks=tasks)
        return tasks
    # (1)
    if not sess or sess.closed:
        sess=ft.Session()
    # (2)
    if not task_id:
        task_id=os.environ["CGRULER_B3D_CURRENT_TASK_ID"]
    # (3)
    task=sess.query(f"select incoming_links from Task where id={task_id}").one()
    # (4) INNER LINKS
    tasks=[]
    # if not _parent_is_shot(task):
    #     _get_downloadable_incoming_links(task['incoming_links'], tasks=tasks)
    _get_downloadable_incoming_links(task['incoming_links'], tasks=tasks)
    # (5) OUTER LINKS
    _get_downloadable_incoming_links(task['parent']['incoming_links'], tasks=tasks)

    # (6)
    components=[]
    volume=0
    for task in tasks:
        size=0
        url=None
        b, vers = get_latest_version_of_task(task_id=task["id"], sess=sess, close_session=False, f=True, startswith="Commit_")
        status="missing"
        if b:
            b,r=db.exists_loaded_version(vers)
            if b:
                status=r[0]
                if r[0] in ("missing", "old"):
                    b, comp = get_task_data_component(vers["id"], sess=sess, close_session=False)
                    if b:
                        size=comp['size']
                        url=comp['component_locations'][0]['url']['value']
        else:
            vers=None
        components.append((task, vers, size, url, status))
        volume+=size
        if vers:
            vers_id=vers.get('id')
        else:
            vers_id="None"
        print(f"INFO: append to incoming links: [{task['parent']['name']}] {task['name']} - vers: {vers_id} - {status}")

    # print(components)
    print(f"INFO: time: {time.time() - timing} sec.")
    return(True, (components, volume))

def get_task_data_component(version_id, sess=False, close_session=True, component_name=TASK_DATA_COMPONENT) -> tuple:
    """Получение компонента версии, по умолчанию - :attr:`TASK_DATA_COMPONENT`, но можно передать любое имя компонента.

    Parameters
    ----------
    version_id : str
        Id версии
    sess : ftrack.Session optional
        Сессия ftrack, если не передавать будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    component_name : str optional
        Имя компонента, по умолчанию :attr:`TASK_DATA_COMPONENT`

    Returns
    -------
    tuple
        (True, component) или (False, comment)
    """
    if not sess or sess.closed:
        sess=ft.Session()
    try:
        components=sess.query(f"Component where version_id = {version_id} and name='{component_name}'")
        if not components:
            return (False, "There is no download data in this version!")
        if close_session:
            sess.close()
    except Exception as e:
        print('***')
        print(e)
        if not sess.closed and close_session:
            sess.close()
        return(False, 'The problem of getting a list of components! Look the terminal.')
    
    return(True, components[0])

def get_url_of_task_data_component(version_id, sess=False, close_session=True):
    """Получение url компонента ``task_data``.

    .. note:: Локация не выбирается, используется по умолчанию ftrack.server

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b,r = get_task_data_component(version_id, sess=sess, close_session=False)
    if not b:
        return(b,r)

    path=r['component_locations'][0]['url']['value']
    
    return(True, path)


def get_url_of_review_component(version_id, sess=False, close_session=True):
    """Получение url ревью компонента.

    .. note:: Локация не выбирается, используется по умолчанию ftrack.server

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    b,r = get_task_data_component(version_id, sess=sess, close_session=False, component_name=REVIEW_COMPONENT)
    if not b:
        return(b,r)

    path=r['component_locations'][0]['url']['value']
    
    return(True, path)


def task_status_to_in_progress(task, sess=False, close_session=True):
    """Меняет статус задачи на ``In progress``, если у данного пользователя есть другие задачи в этом статусе, то их
    статус меняется на ``On Hold``.

    .. note:: Так же запускает таймер задачи.
    
    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    if task['status']['name']=="In progress":
        timer_start()
        return (True, "Ok!")

    if not sess or sess.closed:
        sess=ft.Session()

    project_id=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]

    progress_status = sess.query("Status where name = 'In progress'").one()
    hold_status = sess.query("Status where name = 'On Hold'").one()

    tasks = sess.query(f"select status from Task where assignments any (resource.username = \"{sess.api_user}\") "
        f"and project_id = {project_id} and status.name = 'In progress'")

    for tsk in tasks:
        tsk['status']=hold_status

    t = sess.get("Task", os.environ["CGRULER_B3D_CURRENT_TASK_ID"])
    t['status']=progress_status

    # (start timer)
    timer_start(task=t, sess=sess)

    sess.commit()
    if not sess.closed and close_session:
        sess.close()
    return (True, "Ok!")

def task_status_to(task, status_name: str, sess=False, close_session=True) -> tuple:
    """Замена статуса текущей задачи.

    .. note:: Статус ``In progress`` будет перенаправлен в :func:`task_status_to_in_progress`.

    Returns
    -------
    tuple
        (True, path) или (False, comment)
    """
    #(1)
    if status_name=="In progress":
        return task_status_to_in_progress(task, sess=sess, close_session=close_session)
    #(2)
    if not sess or sess.closed:
        sess=ft.Session()
    #(3)
    t = sess.get("Task", task["id"])
    status = sess.query(f'Status where name = "{status_name}"').one()
    t["status"]=status
    sess.commit()
    #(4)
    if not sess.closed and close_session:
        sess.close()
    return (True, "Ok!")

def timer_start(task=False, sess=False):
    """Запуск таймера задачи. """
    try:
        if not sess or sess.closed:
            sess=ft.Session()
            task = sess.get("Task", os.environ["CGRULER_B3D_CURRENT_TASK_ID"])
        if not task:
            task = sess.get("Task", os.environ["CGRULER_B3D_CURRENT_TASK_ID"])
        user=sess.query(f'User where username = "{sess.api_user}"').one()
        user.start_timer(task, force=True)
        return(True, "Ok!")
    except Exception as e:
        print(f"Exception in timer_start():\n{e}")
        return(False, str(e))

def timer_stop(sess=False):
    """Остановка таймера задачи. """
    def _timer_stop(sess):
        print("_timer_stop")
        try:
            if not sess or sess.closed:
                sess=ft.Session()
            user=sess.query(f"User where username = \"{sess.api_user}\"").one()
            timelog = user.stop_timer()
            return(True, "Ok!")
        except Exception as e:
            print(f"Exception in timer_stop():\n{e}")
            return(False, str(e))

    _timer_stop(sess)
    # t = threading.Thread(target=_timer_stop, args=(sess,), kwargs={})
    # t.setDaemon(True)
    # t.start()

def push_video_to_ftrack(task, path, description, status_name="In progress", asset_type_name="Video", prefix="Review", sess=False, close_session=True, location="ftrack.server"):
    """Загрузка плейбласта на Ftrack. 

    Parameters
    ----------
    task : Task
        текущая задача
    path : str
        путь до видео файла.
    description : str
        комментарий к данной версии.
    status_name : str optional
        Имя статуса, по умолчанию *In progress*.
    asset_type_name : str
        имя типа ассета, по умолчанию *Video*
    prefix : str
        префикс имени ассета
    sess : Session
        сессия, если не передавать, то будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    location : str
        имя локации ftrack.

    Returns
    -------
    tuple
        (True, "ok") или (False, comment)
    """
    if not sess or sess.closed:
        sess=ft.Session()

    try:
        #(1)
        type_parent=task["parent"]["object_type"]["name"].replace(' ', '')
        print(f"{'*'*5} {type_parent}")

        #(2)
        asset_parent = sess.get(type_parent, task["parent"]["id"])
        print(f"{'*'*5} {asset_parent['name']}")

        #(3)
        asset_type = sess.query(f'AssetType where name is "{asset_type_name}"').one()
        print(f"{'*'*5} {asset_type['name']}")

        #(4)
        asset_name = f'{prefix}_{task["parent"]["name"]}_{task["name"].replace(" ", "_")}'
        assets = sess.query(f"Asset where name is {asset_name} and context_id is {asset_parent['id']}")
        print(f"{'*'*5} assets1")
        if assets:
            asset = assets[0]
        else:
            asset = sess.create('Asset', {
                'name': asset_name,
                'type': asset_type,
                'parent': asset_parent
            })
        print(f"{'*'*5} assets2")

        #(5)
        asset_version = sess.create('AssetVersion', {
            'asset': asset,
            'task': task,
            'comment': description
        })
        try:
            status = sess.query(f"Status where name = '{status_name}'").one()
            asset_version["status"]=status
        except Exception as e:
            print(e)
            return(False, f'{e}')
        sess.commit()
        print(f"{'*'*5} assets3")

        #(6)
        loc = sess.query(f'Location where name="{location}"').one()
        print(f"{'*'*5} {loc['name']}")
        
        component = asset_version.create_component(
            path,
            data={
                'name': 'ftrackreview-mp4-1080'
            },
            location=loc
        )
        component['name']=REVIEW_COMPONENT
        component['metadata']['ftr_meta'] = json.dumps({
            'frameIn': 1,
            'frameOut': bpy.context.scene.frame_end - bpy.context.scene.frame_start,
            'frameRate': int(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_FPS"])),
            'height': int(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_HEIGHT"])),
            'width': int(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_WIDTH"]))
        })
        sess.commit()

        return(True, "Ok!")
    except Exception as e:
        print(f"Exception in push_video_to_ftrack(): {e}")
        return(False, f'{e}')


def push_version_to_ftrack(task, paths, version, description, sess=False, close_session=True, location="ftrack.server", status_name="In progress") -> tuple:
    """Загрузка версии на Ftrack. 

    .. note:: Тип актива определяется автоматом от типа задачи.

    Parameters
    ----------
    task : Task
        текущая задача
    paths : list
        список путей до файлов, которые будут загружены с версией.
    version : int
        номер локальной версии.
    description : str
        комментарий к данной версии.
    sess : Session
        сессия, если не передавать, то будет создана.
    close_session : bool
        Если True, то сессия будет закрыта по завершению запроса.
    location : str
        имя локации ftrack.
    status_name : str optional
        Имя статуса, по умолчанию *In progress*.
    """
    if not sess or sess.closed:
        sess=ft.Session()

    try:
        type_parent=task["parent"]["object_type"]["name"].replace(' ', '')
        print(type_parent)

        asset_parent = sess.get(type_parent, task["parent"]["id"])
        # GET ASSET_TYPE
        if task["type"]["name"] in ("Modeling", "Lookdev", "Shading"):
            asset_type = sess.query('AssetType where name is "Geometry"').one()
        elif task["type"]["name"]=="Rigging":
            asset_type = sess.query('AssetType where name is "Rig"').one()
        elif task["type"]["name"] in ("Animation", "Layout"):
            asset_type = sess.query('AssetType where name is "Animation"').one()
        else:
            asset_type = sess.query('AssetType where name is "Upload"').one()
        # GET ASSET
        asset_name = f'Commit_{task["name"]}'
        assets = sess.query(f"Asset where name is {asset_name} and context_id is {asset_parent['id']}")
        if assets:
            asset = assets[0]
        else:
            asset = sess.create('Asset', {
                'name': asset_name,
                'type': asset_type,
                'parent': asset_parent
            })
        # CREATE VERSION
        asset_version = sess.create('AssetVersion', {
            'asset': asset,
            'task': task,
            'comment': description
        })
        try:
            status = sess.query(f"Status where name = '{status_name}'").one()
            asset_version["status"]=status
        except Exception as e:
            print(e)
        sess.commit()

        loc = sess.query(f'Location where name="{location}"').one()
        print(loc)
        for path in paths:
            component = asset_version.create_component(
                path, location=loc
            )
            component['name']=os.path.splitext(os.path.basename(path))[0]
        sess.commit()
        # ID OF VERSION TO LOCAL BD
        # -- WORK VERSION
        b,r=db.update_work_version(task, version, asset_version["id"])
        if not b:
            print(r)
        # -- LOADED VERSION
        b,r=db.exists_loaded_version(asset_version)
        if not b:
            print(r)
        elif r[0]=="missing":
            b,r=db.add_loaded_version(asset_version)
            if not b:
                print(r)
        elif r[0]=="old":
            b,r=db.update_loaded_version(asset_version)
            if not b:
                print(r)
        # END
        timer_stop(sess=sess)
        timer_start(sess=sess)
        if not sess.closed and close_session:
            sess.close()
        return(True, "Ok!")
    except Exception as e:
        print(e)
        return(False, e)

def get_task_templates(sess=False, close_session=True):
    """Возвращает список TaskTemplate объектов. """
    if not sess or sess.closed:
        b, sess=get_session()
        if not b:
            return(False, sess)

    try:
        templates=sess.query("TaskTemplate")
        sess.populate(templates, 'name')
    except Exception as e:
        closing_session(sess)
        return(False, f"exception in get_task_templates(): \n{str(e)}")

    #(fin)
    if close_session:
        closing_session(sess)
    return(True, templates)


#### Utilites

def clean_final_slash(path):
    while path.endswith('/'):
        path=path[:-1]
    return path