# -*- coding: utf-8 -*-

"""Настройка рабочей среды пользователя."""

import os
import json
import traceback

try:
    from config import *
except:
    from .config import *

try:
    from ..local_config import *
except:
    print(f'{traceback.format_exc()}')

def _get_data_folder(create=True) -> str:
    """Возвращает путь до директории где хранятся настройки, при отсутствии создаёт.

    Parameters
    ----------
    create : bool, optional
        Если ``True`` то создаст при отсутствии.

    Returns
    -------
    str
        path.
    """
    folder_path=os.path.join(os.path.expanduser('~'), DATA_FOLDER)
    if not os.path.exists(folder_path):
        if create:
            try:
                os.makedirs(folder_path)
            except Exception as e:
                print(f'{traceback.format_exc()}')
                return None
        else:
            return None
    return folder_path


def _get_auth_data_file():
    """Возвращает путь до :attr:`config.AUTH_FILE` """
    return os.path.join(_get_data_folder(), AUTH_FILE)


def _get_cookie_path():
    '''
    Возвращает путь до файла ``cookie``, создаёт при остутствии.

    Returns
    -------
    path
        Путь до файла ``cookie``.
    '''
    path=os.path.join(_get_data_folder(), COOKIE_NAME)
    if not os.path.exists(os.path.dirname(path)):
        os.makedirs(path, mod=448)
    return path


def _write_setting_data(**kw) -> None:
    """Записывает данные в файл :attr:`config.SETTING_FILE` 

    Parameters
    ----------
    kw : dict
        словарь в именованные аргументы, запишет все значения в словарь по ключам-(именам аргументов).

    Returns
    -------
    None
    """
    # path to file
    path = os.path.join(_get_data_folder(), SETTING_FILE)
    # get data
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            data=json.load(f)
    else:
        data=dict()
    # append data
    for key in kw:
        data[key]=kw[key]
    # write data
    try:
        with open(path, 'w', encoding='utf-8') as f:
            json.dump(data, f)
    except Exception as e:
        print(f'{traceback.format_exc()}')

def _read_setting_data(key=False):
    """Читает данные из файла :attr:`config.SETTING_FILE`

    Parameters
    ----------
    key : str, optional
        *   Если передать ключ - то вернёт только значение по этому ключю.
        *   Если не передавать, то будет возвращён весь словарь.

    Returns
    -------
    dict, optional
    """
    # path to file
    path = os.path.join(_get_data_folder(), SETTING_FILE)
    # get data
    if os.path.exists(path):
        with open(path, 'r', encoding='utf-8') as f:
            data=json.load(f)
    else:
        data=dict()
    # return data
    if key:
        return data.get(key)
    else:
        return data

def get_projects_folder() -> str:
    """Возвращает директорию для размещения проектов, чтение из :attr:`config.SETTING_FILE` """
    path = _read_setting_data("projects_folder")
    try:
        if os.path.exists(path):
            os.environ['CGRULER_B3D_PROJECTS_DIR']=path
            return path
        else:
            # os.environ['CGRULER_B3D_PROJECTS_DIR']=None
            return 'Undefined'
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return 'Undefined'

def set_projects_folder(path: str) -> tuple:
    """Содание директории для размещения проектов, запись пути в :attr:`config.SETTING_FILE`. 

    Parameters
    ----------
    path : str
        Путь директории где будет создана :attr:`config.PROJECTS`.

    Returns
    -------
    tuple
        (True, path - путь до созданной :attr:`config.PROJECTS`) или (False, comment)
    """
    if not os.path.exists(path):
        return (False, f'the path does not exist: {path}')
    # --
    # print(f'basename ({path}) - {os.path.basename(clean_final_slash(path))}')
    if os.path.basename(clean_final_slash(path))==PROJECTS:
        projects_path=path
    else:
        projects_path = os.path.join(path, PROJECTS)
    # print(f'projects_path - {projects_path}')
    # --
    try:
        if not os.path.exists(projects_path):
            os.makedirs(projects_path)
        _write_setting_data(projects_folder=projects_path)
        os.environ['CGRULER_B3D_PROJECTS_DIR']=projects_path
        return (True, projects_path)
    except Exception as e:
        print(f'{traceback.format_exc()}')
        return (False, f"{e}")

def read_auth_data() -> dict:
    """Чтение параметров для аутентификации из файла :attr:`config.AUTH_FILE`

    Returns
    -------
    dict
        {'username': '', 'password': ''}
    """

    data=dict()
    file_path=_get_auth_data_file()
    if os.path.exists(file_path):
        with open(file_path, 'r', encoding='utf-8') as f:
            data=json.load(f)
    return data


def add_graphics_editors(name: str, path: str) -> None:
    """Добавление графического редактора в список (пути до экзешника или команды терминала).

    Parameters
    ----------
    name : str
        псевдоним для данного редактора.
    path : str
        путь до экзешника или команда терминала запускающая софт.

    Returns
    -------
    None
    """
    r=_read_setting_data(key="graphics_editors")
    if r:
        r[name]=path
    else:
        r={name:path}

    return _write_setting_data(graphics_editors = r)

def del_graphics_editors(name: str) -> None:
    """Удаление графического редактора из списка (пути до экзешника или команды терминала).

    Parameters
    ----------
    name : str
        псевдоним для данного редактора.

    Returns
    -------
    None
    """
    r=_read_setting_data(key="graphics_editors")
    if r and (name in r):
        del r[name]
    
    return _write_setting_data(graphics_editors = r)


def read_graphics_editors() -> dict:
    """Чтение словаря графических редакторов (пути до экзешников или команды терминала)

    Returns
    -------
    dict
        {data} или {empty}
    """
    return _read_setting_data(key="graphics_editors")


#### Utilites

def clean_final_slash(path):
    while path.endswith('/'):
        path=path[:-1]
    return path