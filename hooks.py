# -*- coding: utf-8 -*-

"""Хуки связанные с событиями open, commit, ... итд. """

import os

import bpy

try:
    from pycgruler import cgruler
except:
    from .pycgruler import cgruler

def _set_render_setting_for_playblast(fstart=None, fend=None, filepath=None):
    """Выставляет настройки для плейбласта. 

    Parameters
    ----------
    fstart : str, float, int, optional
        Значение для bpy.context.scene.fstart:

        * None - устанавливаться не будет.
        * float, int - будет установлено это значение.
        * 'ENV' - значение будет взято из переменной окружения :ref:`CGRULER_B3D_CURRENT_PROJECT_FSTART`

    fend : str, float, int, optional
        Значение для bpy.context.scene.fend:

        * None - устанавливаться не будет.
        * float, int - будет установлено это значение.
        * 'ENV' - значение будет взято из переменной окружения :ref:`CGRULER_B3D_SCENE_FEND`.

    filepath : str, optional

        * Путь сохранения видеофайла, параметр для *bpy.context.scene.render.filepath*
        * Если не передавать значение будет взято из :ref:`CGRULER_B3D_CURRENT_ASSET_NAME` + ".mp4"

    Returns
    -------
    None
    """
    try:
        if fstart is not None:
            if isinstance(fstart, int) or isinstance(fstart, int):
                bpy.context.scene.frame_start=fstart
            elif fend=="ENV":
                bpy.context.scene.frame_start=float(os.environ.get('CGRULER_B3D_CURRENT_PROJECT_FSTART'))
        if fend is not None:
            if isinstance(fend, int) or isinstance(fend, float):
                bpy.context.scene.frame_end=fend
            elif fend=="ENV":
                bpy.context.scene.frame_end=float(os.environ.get('CGRULER_B3D_SCENE_FEND'))
        #
        bpy.context.scene.render.resolution_x=int(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_WIDTH"]))
        bpy.context.scene.render.resolution_y=int(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_HEIGHT"]))
        bpy.context.scene.render.pixel_aspect_x=1
        bpy.context.scene.render.pixel_aspect_y=1
        bpy.context.scene.render.fps=int(float(os.environ["CGRULER_B3D_CURRENT_PROJECT_FPS"]))
        bpy.context.scene.render.fps_base=1
        if filepath is None:
            bpy.context.scene.render.filepath=f'//{os.environ["CGRULER_B3D_CURRENT_ASSET_NAME"]}.mp4'
        else:
            bpy.context.scene.render.filepath=filepath
        bpy.context.scene.render.image_settings.file_format="FFMPEG"
        bpy.context.scene.render.ffmpeg.format="MPEG4"
        bpy.context.scene.render.ffmpeg.codec="H264"
        bpy.context.scene.render.ffmpeg.constant_rate_factor="VERYLOW"
        bpy.context.scene.render.ffmpeg.ffmpeg_preset="GOOD"
        bpy.context.scene.render.ffmpeg.gopsize=15
        bpy.context.scene.render.ffmpeg.use_max_b_frames=False
        bpy.context.scene.render.ffmpeg.audio_codec="MP3"
        bpy.context.scene.render.ffmpeg.audio_channels="STEREO"
        bpy.context.scene.render.ffmpeg.audio_mixrate=48000
        bpy.context.scene.render.ffmpeg.audio_bitrate=192
        bpy.context.scene.render.ffmpeg.audio_volume=1
        bpy.context.scene.render.image_settings.color_mode="RGB"
        bpy.context.scene.render.use_render_cache=False
        bpy.context.scene.render.use_file_extension=True
        #(METADATA)
        bpy.context.scene.render.metadata_input="SCENE"
        bpy.context.scene.render.use_stamp=True
        bpy.context.scene.render.stamp_font_size=48
        bpy.context.scene.render.use_stamp_date=False
        bpy.context.scene.render.use_stamp_time=False
        bpy.context.scene.render.use_stamp_render_time=False
        bpy.context.scene.render.use_stamp_frame=True
        bpy.context.scene.render.use_stamp_frame_range=False
        bpy.context.scene.render.use_stamp_memory=False
        bpy.context.scene.render.use_stamp_hostname=False
        bpy.context.scene.render.use_stamp_camera=False
        bpy.context.scene.render.use_stamp_lens=False
        bpy.context.scene.render.use_stamp_scene=False
        bpy.context.scene.render.use_stamp_marker=False
        bpy.context.scene.render.use_stamp_filename=False
        bpy.context.scene.render.use_stamp_sequencer_strip=True
        bpy.context.scene.render.use_stamp_labels=False
    except Exception as e:
        print(f"{'#'*50}\n{e}")
    else:
        print(f'{"#"*50} _set_render_setting_for_playblast()')


def post_open(task, look):
    """
    Действия после открытия рабочей сцены.

    Parameters
    ----------
    task : task
        Текущая задача.
    look : bool
        Если look=True то статус задачи меняться не будет.

    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    #(fix ui)
    bpy.context.scene.cgruler_auth_current_user = os.environ["CGRULER_B3D_AUTH_USER"]
    bpy.context.scene.cgruler_projects=os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]
    if os.environ.get("CGRULER_B3D_ROLE"):
        bpy.context.scene.cgruler_role=os.environ.get("CGRULER_B3D_ROLE")

    #(render settings)
    if task["process"]["name"] in cgruler.settings.ANIMATION_TASK_PROCESSES:
        _set_render_setting_for_playblast()

    #(relative paths)
    bpy.ops.file.make_paths_relative()

    #(chasnge status)
    if look:
        return(True, "Ok!")
    else:
        return cgruler.task_set_status(task, "Working")


def pre_commit(task, description, status_name):
    bpy.ops.file.make_paths_relative()
    return(True, "Ok!")