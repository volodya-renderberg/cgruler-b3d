# -*- coding: utf-8 -*-

"""Локальная организация работы с ассетами. Ведение журналов. Рабочие и бекап версии. """

import os
import json
import datetime
import webbrowser
import time
import threading
import tempfile
import subprocess
# import platform
import shutil
from zipfile import ZipFile
import ssl
from urllib.request import urlretrieve
import logging
import traceback

import bpy

try:
    from pycgruler import path_utils
    from pycgruler import settings
    from pycgruler import db
    import hooks
    from pycgruler import server_connect as srvconn
    from pycgruler import cgruler
except:
    from .pycgruler import path_utils
    from .pycgruler import settings
    from .pycgruler import db
    from . import hooks
    from .pycgruler import server_connect as srvconn
    from .pycgruler import cgruler


# logging.basicConfig(
#     filename=os.path.join(tempfile.gettempdir(),'ftrack_log'),
#     filemode='a',
#     format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
#     datefmt='%H:%M:%S',
#     level=logging.INFO)


EXCLUDED_DIRECTORIES=[".backup", "versions"]
"""Имена директорий, которые не загружаются на Ftrack. """

SUPPORTED_VIDEO_EXTENSIONS=[
        ".mp4",
    ]
"""Форматы видеофайлов поддерживаемые для загрузки аниматика. """

ORDER_OF_ENTITIES=("Episode", "Sequence", "Shot")


def __test_is_video(filepath) -> bool:
    """Является ли файл видеофайлом. """
    if os.path.splitext(filepath)[1].lower() in SUPPORTED_VIDEO_EXTENSIONS:
        return True
    else:
        print(os.path.splitext(filepath)[1].lower())
        return False


def _get_sequencer(context):
    """Делает проверку на наличие и возвращает секвенсор.

    Parameters
    ----------
    context : bpy.context
        КОнтекст блендера

    Returns
    -------
    tuple
        (True, context) или (False, comment)
    """
    area=None
    for area in context.window.workspace.screens[0].areas:
        if area.type == "SEQUENCE_EDITOR":
            break
    if not area:
        return(False, "No found SEQUENCE_EDITOR!")
    sequencer = context.scene.sequence_editor
    return(True, sequencer)


def _get_selected_shots(context):
    """
    Возвращает имена выделенных секвенций шотов в секвенсоре.
    * Игнорирует аниматик,
    * Преобразует ``.sound`` и ``.review`` в шот,
    * Удаляет совпадения имён.
    """
    sequences=list()
    for seq in bpy.context.selected_sequences:
        if not seq.name.startswith("Shot"):
            continue
        else:
            # sequences.append(seq.name.replace('.sound', ''))
            sequences.append(seq.name.split('.')[0])
    set_seq=set(sequences)
    return sorted(list(set_seq))


def _get_all_shots(context):
    """
    Возвращает имена всех секвенций шотов в секвенсоре.
    * Игнорирует аниматик,
    * Преобразует ``.sound`` и ``.review`` в шот,
    * Удаляет совпадения имён.
    """
    sequences=list()
    for seq in bpy.context.scene.sequence_editor.sequences_all:
        if not seq.name.startswith("Shot"):
            continue
        else:
            # sequences.append(seq.name.replace('.sound', ''))
            sequences.append(seq.name.split('.')[0])
    set_seq=set(sequences)
    return sorted(list(set_seq))


def _get_shot_sequences(context, shot_name):
    """Возвращает кортеж секвенций связанных с данным шотом.

    Parameters
    ----------
    shot_name : str
        Название шота.

    Returns
    -------
    tuple
        (sequence1, sequence2, ....).
    """
    data=list()
    for postfix in ('', '.review', '.sound'):
        if f"{shot_name}{postfix}" in context.scene.sequence_editor.sequences_all:
            data.append(context.scene.sequence_editor.sequences_all[f"{shot_name}{postfix}"])
    return(data)


def _download_last_assetversion_move(sess, shot_name, startswith=False, shot_id=False, after=False):
    """Загрузка видеофайла последней версии превью или аниматика в tmp директорию.

    Parameters
    ----------
    sess : ftrack.sess
        Сессия Ftrack
    shot_name : str
        наименование шота, берётся из названия секвенции.
    startswith : str, optional
        Префикс имена ассета версии, Если не передавать - "Review\_".
    shot_id : str, optional
        **id** шота, если передать то **shot_name** не нужен, можно передать любое значение.
    after : str
        Дата версии(iso) от которой идёт поиск.

    Returns
    -------
    tuple
        (download_path - путь загрузки мувки, shot - объект шота.)
    """

    #(shot_ft)
    if not shot_id:
        b,shot_ft=srvconn.get_asset_by_name(shot_name, "Shot", sess=sess, close_session=False)
        if not b:
            raise Exception(str(shot_ft))
        if not shot_ft:
            raise Exception(str(f"Shot named \"{shot_name}\" does not exist on the server!"))
    else:
        shot_ft=dict()
        shot_ft['id']=shot_id

    #(animatic_ft)
    b,animatic_ft=srvconn.get_reviews_list_of_asset(shot_ft['id'], last=True, startswith=startswith, sess=sess, close_session=False, after=after)
    if not b:
        raise Exception(str(animatic_ft))
    #(-- download review file)
    b,url=srvconn.get_url_of_review_component(animatic_ft['id'], sess=sess, close_session=False)
    if not b:
        raise Exception(str(url))
    b,download_path=cgruler._download_file_from_url(url, f"{srvconn.REVIEW_COMPONENT}.mp4")
    if not b:
        raise Exception(f'*** {download_path} ***')

    logging.info(f'downloaded {startswith}')
    return (download_path, animatic_ft)


def refresh_proxy(context):
    """
    Пересоздаёт выделенный прокси,
    с сохранением положения и анимационного экшена.
    Копирует ``child_off`` констрейн на **root** контрол и на сам прокси объект(если есть).
    """
    #(get data)
    ob_data={}
    ob_child_ofs={}
    root_child_ofs={}
    objects=[]
    for ob in context.selected_objects:
        objects.append(ob.name)
    for name in objects:
        ob=bpy.data.objects[name]
        #(ob_data)
        ob_data[ob.name]={}
        if ob.proxy:
            ob_data[ob.name]["original"]=ob.proxy
        else:
            ob_data[ob.name]["original"]=None
        ob_data[ob.name]["loc"]=ob.location[:]
        ob_data[ob.name]["rot"]=ob.rotation_euler[:]
        ob_data[ob.name]["scl"]=ob.scale[:]
        if ob.animation_data:
            ob_data[ob.name]["action"]=ob.animation_data.action
        else:
            ob_data[ob.name]["action"]=None
        #(object CHILD_OF)
        ob_child_ofs[ob.name]=[]
        for constr in ob.constraints:
            if constr.type=="CHILD_OF":
                ob_child_ofs[ob.name].append({
                    "name":constr.name,
                    "target":constr.target.name,
                    "subtarget":constr.subtarget,
                    })
                ob.constraints.remove(constr)
        #(root bone CHILD_OF)
        ob.select_set(True)
        context.view_layer.objects.active = ob
        bpy.ops.object.mode_set(mode='POSE')
        pose_root=None
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
        if pose_root:
            root_child_ofs[ob.name]=[]
            for constr in pose_root.constraints:
                if constr.type=="CHILD_OF":
                    root_child_ofs[ob.name].append({
                        "name":constr.name,
                        "target":constr.target.name,
                        "subtarget":constr.subtarget,
                        })
                    pose_root.constraints.remove(constr)
    #(remove)
    for name in objects:
        ob=bpy.data.objects[name]
        if ob_data[name]["original"]:
            ob.select_set(True)
            context.view_layer.objects.active = ob
            bpy.ops.object.mode_set(mode='OBJECT')
            bpy.data.objects.remove(ob, do_unlink=True)

    tmpdir=tempfile.gettempdir()
    with open(os.path.join(tmpdir,'ob_data.json'), 'w') as f:
        json.dump(str(ob_data), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'ob_child_ofs.json'), 'w') as f:
        json.dump(str(ob_child_ofs), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'root_child_ofs.json'), 'w') as f:
        json.dump(str(root_child_ofs), f, sort_keys=True, indent=4)
    with open(os.path.join(tmpdir,'objects.json'), 'w') as f:
        json.dump(str(objects), f, sort_keys=True, indent=4)
        
    #(create proxy)
    for name in objects:
        if "original" in ob_data[name]:
            original=ob_data[name]["original"]
            original.select_set(True)
            context.view_layer.objects.active = original
            #(create proxy)
            bpy.ops.object.proxy_make(object='DEFAULT')
            proxy=bpy.data.objects[f"{original.name}_proxy"]
            #(-- hide original)
            original.select_set(False)
            original.hide_set(True)
            #(-- activate proxy)
            proxy.select_set(True)
            context.view_layer.objects.active = proxy
            #(-- proxy position)
            proxy.location=ob_data[name]["loc"]
            proxy.rotation_euler=ob_data[name]["rot"]
            proxy.scale=ob_data[name]["scl"]
            #(-- action)
            if ob_data[name]["action"]:
                proxy.animation_data_create()
                proxy.animation_data.action=ob_data[name]["action"]

    #(create constraints)
    for name in objects:
        ob=bpy.data.objects[name]
        ob.select_set(True)
        context.view_layer.objects.active = ob
        #(-- pose constraint)
        bpy.ops.object.mode_set(mode='POSE')
        if "root" in ob.pose.bones:
            pose_root=ob.pose.bones["root"]
            if name in root_child_ofs:
                for item in root_child_ofs[name]:
                    constr=pose_root.constraints.new("CHILD_OF")
                    constr.name=item["name"]
                    # try:
                    # constr.target=item["target"]
                    constr.target=bpy.data.objects[item["target"]]
                    constr.subtarget=item["subtarget"]
                    constr.influence=0
                    # except Exception as e:
                    #     print(e)
                    #     print(f'{"*"*50} {item["target"]} {item["subtarget"]}')
        #(-- ob constraint)
        bpy.ops.object.mode_set(mode='OBJECT')
        if name in ob_child_ofs:
            for item in ob_child_ofs[name]:
                constr=ob.constraints.new("CHILD_OF")
                constr.name=item["name"]
                # try:
                # constr.target=item["target"]
                constr.target=bpy.data.objects[item["target"]]
                constr.subtarget=item["subtarget"]
                constr.influence=0
                # except Exception as e:
                #     print(e)
                #     print(f'{"*"*50} {item["target"]} {item["subtarget"]}')
        
        return(True, "Ok!")


def save_current_scene_as_top_version(task) -> tuple:
    """Сохранение текущей сцены в топ версию, с изменением статуса в ``in_progress``

    Кроме сохранения самой сцены, забираются её текстуры из /textures, 
    а так же находящихся за пределами :ref:`CGRULER_B3D_PROJECTS_DIR`.
    """

    #(список сохраняемых текстур)
    collecting_images=list()
    try:
        b1,project_dir=path_utils.get_project_folder_path(task)
        if not b1:
            raise Exception(project_dir)
        textures=os.path.join(os.path.dirname(bpy.data.filepath), settings.TEXTURES_FOLDER)
        for image in bpy.data.images:
            img_path=image.filepath_from_user()
            #(in textures)
            if bpy.path.is_subdir(img_path, textures):
                collecting_images.append((image, img_path))
            #(outer project dir)
            elif b1 and not bpy.path.is_subdir(img_path, project_dir) and os.path.exists(img_path):
                collecting_images.append((image, img_path))
    except Exception as e:
        print(f"WARNING: problem with collecting textures in save_current_scene_as_top_version():\n{e}")
        print(f'{traceback.format_exc()}')

    #(save file)
    b,r = path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False,r)
    if bpy.data.filepath != r:
        try:
            bpy.ops.wm.save_as_mainfile(filepath = r, check_existing = True)
        except Exception as e:
            return(False, f'{e}')

        #(копирование сохраняемых текстур)
        b,textures=path_utils.get_folder_of_textures_path(task)
        if not b:
            return(b,textures)
        for item in collecting_images:
            new_path=os.path.join(textures, os.path.basename(item[1]))
            shutil.copyfile(item[1], new_path)
            item[0].filepath=new_path
            print(f"{item} - {new_path}")

    # return(True, "Ok!")
    return hooks.post_open(task, False)


def open_scene_from_incoming_task(task, incoming_task):
    """Взятие в работу файла входящей задачи.

    * Копирование файла в топ версию.
    * Изменением статуса в ``in_progress``.

    Parameters
    ----------
    task : ftrack.Task
        Текущая задача.
    incoming_task : ftrack.Task
        Входящая задача, чей файл забираем.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b,path = path_utils.get_top_version_path_of_workfile(incoming_task)
    if not b:
        return(False,path)
    if not os.path.exists(path):
        return(False, 'File is missing!')
    else:
        bpy.ops.wm.open_mainfile(filepath = path)

    # return (True, f"Opened from {incoming_task['id']}")
    b,r=path_utils.check_file_structure(task)
    if not b:
        return(b,r)
    return save_current_scene_as_top_version(task)


def open(task, look=False) -> tuple:
    """Открытие top work версии. 

    .. note:: Если look=True то статус задачи меняться не будет.
    """
    b,path = path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False,path)
    if not os.path.exists(path):
        return(False, 'File is missing!')
    else:
        bpy.ops.wm.open_mainfile(filepath = path)
        return hooks.post_open(task, look)


def open_version(task, version, look=False) -> tuple:
    """Открытие work версии по номеру. 

    .. note:: Если look=True то статус задачи меняться не будет.
    """
    b,tpath= path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(False,tpath)

    b,vpath = path_utils.get_version_path_of_workfile(task, version)
    if not b:
        return(False,vpath)
    if not os.path.exists(vpath):
        return(False, 'File is missing!')
    else:
        #(old)
        # bpy.ops.wm.open_mainfile(filepath = vpath)
        # bpy.ops.wm.save_as_mainfile(filepath = tpath, check_existing = True)
        #(new)
        shutil.copyfile(vpath, tpath)
        bpy.ops.wm.open_mainfile(filepath = tpath)
        return hooks.post_open(task, look)


def lag_test_of_version(task) -> tuple:
    """
    Тест отставания локальной версии (выбор последней по дате между созданной и загруженной) от последней версии на сервере.

    Parameters
    ----------
    task : task
        Объект задачи.
    
    Returns
    -------
    tuple
        (True, num: int - количество опережающих версий, 0 - значит версия свежая)  или (False, comment)
    """
    b1,r1=srvconn.get_versions_list(task_id=task["id"], startswith="Commit_")
    b2,r2=db.get_latest_work_version(task)
    b3,r3=db.get_latest_loaded_version(task)

    if not all((b1,b2,b3)):
        return(False, (r1,r2,r3))

    r1=list(r1[0])
    if not r1:
        lag=0
    if not any((r2, r3)):
        lag=len(r1)
    else:
        if not r2 and r3:
            version_id=dict(r3)["version_id"]
        elif not r3 and r2:
            version_id=dict(r2)["version_id"]
        else:
            d2=dict(r2)
            d3=dict(r3)
            if d2["version_id"] != d3["version_id"]:
                if d2["created"]>d3["created"]:
                    version_id=d2["version_id"]
                else:
                    version_id=d3["version_id"]
            else:
                version_id=d2["version_id"]

        r1.reverse()

        lag=0
        for item in r1:
            if item['id']==version_id:
                break
            else:
                lag+=1

    return(True, lag)


def commit(task, description, status_name, push=True) -> tuple:
    """Создание рабочей версии локально и фоновая выгрузка версии на Фтрек.

    Parameters
    ----------
    task : task
        Текущая задача.
    description : str
        Коментарий к версии.
    status_name : str
        Статус назначаемый задаче.
    push : bool
        Делать пуш версии в облако.

    Returns
    -------
    tuple
        (True, num_version: int) или (False, comment)
    """
    #(modification description)
    if not description:
        return(False, 'Description is a required parameter!')
    else:
        description=f"{description} [{bpy.app.version_string}]"

    #(pre_commit)
    hooks.pre_commit(task, description, status_name)

    #(save file)
    bpy.ops.wm.save_as_mainfile()

    #(commit)
    return cgruler.commit(task, description, status_name, bpy.data.filepath, push=push)

    
def playblast_to_review(task, description, path, status_name):
    # ФОНОВАЯ ВЫГРУЗКА
    # t = threading.Thread(target=_load_video_to_ftrack, args=(task, path, description, status_name), kwargs={})
    # t.setDaemon(True)
    # t.start()
    # return(True, "Ok!")
    if not description:
        return(False, 'Description is a required parameter!')
    return _load_video_to_ftrack(task, path, description, status_name)


def _load_video_to_ftrack(task, path, description, status_name, prefix="Review"):
    return srvconn.push_video_to_ftrack(task, path, description, status_name=status_name, prefix=prefix)


def download_incoming_task_data_component(task, version, url=False, sess=False) -> tuple:
    """Загрузка в топ версию компонента task_data, внесение записи в журнал загруженных компонент.

    Parameters
    ----------
    task : str ftrack.Task optional
        Задача или ``id`` задачи.
    version : ftrack.AssetVersion
        Загружаемая версия.
    url : str optional
        Путь до компонента. Проверяется до проверки ``version_id``.
    
    Returns
    -------
    tuple
        (True, 'Ok') или (False, comment)
    """
    # (get task)
    timing = time.time()
    if isinstance(task, str):
        b,task,sess=srvconn.get_task_by_id(task, close_session=False)
        if not b:
            return(b, task)
    # (0)
    b,r=path_utils.check_file_structure(task)
    if not b:
        return(False,r)
    # (1) DOWNLOAD
    b,dowload_path=cgruler._download_component(url=url, version_id=version["id"], sess=sess)
    if not b:
        return(b,dowload_path)
    # (2) TASK_FOLDER
    b,task_folder=path_utils.get_task_folder_path(task)
    if not b:
        return(False, task_folder)
    # (3) EXTRAKT
    download_dir=os.path.dirname(dowload_path)
    with ZipFile(dowload_path) as myzip:
        myzip.extractall(path=task_folder)
    # (4) LOADED_VERSION
    b,r= db.update_loaded_version(version)
    if not b:
        return(b,r)
    return(True, f"Loaded version({version['id']}) of the: {task['parent']['name']} - {task['name']} ({time.time() - timing} sec.)")


def collect_textures(task) -> tuple:
    """Сборка текстур находящихся за пределами :ref:`CGRULER_B3D_PROJECTS_DIR`,
    в директорию :attr:`path_utils.TEXTURES_FOLDER`

    Parameters
    ----------
    task : ftrack.Task
        Текущая задача.
    
    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    b,textures=path_utils.get_folder_of_textures_path(task)
    if not b:
        return(b,textures)
    b1,project_dir=path_utils.get_project_folder_path(task)
    if not b1:
        return(b1, project_dir)
    for image in bpy.data.images:
        origin_path=image.filepath_from_user()
        # if os.path.dirname(origin_path) != textures:
        if not bpy.path.is_subdir(origin_path, project_dir) and os.path.exists(origin_path):
            try:
                image.pack()
                image.unpack(method='WRITE_LOCAL')
                print(f"Collected image: {os.path.basename(origin_path)}")
            except Exception as e:
                print(f"Exception when collecting image: {origin_path}")
                print(e)

    return (True, "Ok!")


def get_collections_of_source(task) -> tuple:
    """Получение списка коллекций на загрузку. """
    b,filepath=path_utils.get_top_version_path_of_workfile(task)
    if not b:
        return(b,filepath)
    collections=[]
    with bpy.data.libraries.load(filepath, link=False) as (data_from, data_to):
        for name in data_from.collections:
            print(f"collection - {name}")
            collections.append(name)
    return(True, (collections, filepath))


def link_collection(context, filepath: str, collection: str, link=True) -> tuple:
    """Линкование коллекции в сцену. """
    bpy.ops.file.make_paths_absolute()
    try:
        with bpy.data.libraries.load(filepath, link=link) as (data_from, data_to):
            data_to.collections.append(collection)
        if link:
            # if not collection in bpy.data.objects:
            #     ob = bpy.data.objects.new(collection, None)
            # else:
            #     ob = bpy.data.objects[collection]
            # ob.dupli_group = bpy.data.collections[collection]
            # ob.dupli_type = 'GROUP'
            # context.scene.collection.objects.link(ob)
            context.scene.collection.children.link(bpy.data.collections[collection])
        else:
            context.scene.collection.children.link(bpy.data.collections[collection])
    except Exception as e:
        return(False, str(e))
    # bpy.ops.file.make_paths_relative()
    return(True, "Ok!")


def open_images(application, filepath):
    if not os.path.exists(filepath):
        return(False, f"path not found - {filepath}")
    try:
        cmd = [application, filepath]
        print(f"*** {cmd}")
        subprocess.Popen(cmd)
    except Exception as e:
        print(e)
        return(False, str(e))
    
    return(True, 'Ok!')


def append_selected_link(context, ob):
    """Замена линкованного объекта, с удалением, на append. """
    if not ob.proxy:
        return(False, "No proxy")
    #(position)
    locations=ob.location[:]
    rotations=ob.rotation_euler[:]
    scales=ob.scale[:]
    ob_name=ob.proxy.name
    #(library)
    lib=None
    for l in bpy.data.libraries:
        if ob.proxy in l.users_id:
            lib=l
    if not lib:
        return(False, "No lib")
    path=lib.filepath
    #(get root collection of library)
    
    return(True, path)


def _get_entity_name_type(context) -> tuple:
    """Возвращает имя и тип сущности из (Episode, Sequence, Shot) по выбранной дорожке в **Sequence_editor**

    .. note:: Выбирать надо одну дорожку.

    Returns
    -------
    tuple
        (True, {"entity_name": entity_name, "entity_type": entity_type}) или (False, comment)
    """
    if not context.selected_sequences or len(context.selected_sequences) >1:
        return(False, "You have to choose ONE track")
    name=context.selected_sequences[0].name
    if name.endswith(".sound"):
        entity_name=name[:-6]
    else:
        entity_name=name
    if name.lower().startswith("episode"):
        entity_type="Episode"
    elif name.lower().startswith("sequence"):
        entity_type="Sequence"
    elif name.lower().startswith("shot"):
        entity_type="Shot"
    else:
        return(False, f'Undefined type of entity: "{name}"')
    
    return(True, {"entity_name": entity_name, "entity_type": entity_type})


def download_animatic(context, task, filepath):
    """
    * Загрузка видео файла аниматика из указанного пути в директорию задачи и размещение в ``sequence_editor`` на первых двух дорожках:
        * дорожка 1 - мувик, 
        * дорожка 2 - sound.
    * Установка тайминга сцены по диапазону аниматика.
    """
    #(TEST FILE)
    if not __test_is_video(filepath):
        return(False, f"The format of the selected file is not supported! Enum in: {str(SUPPORTED_VIDEO_EXTENSIONS)}")

    #(SCENE HIERARCHY)
    if context.scene.name==task["parent"]["name"]:
        pass
    elif len(bpy.data.scenes)==1 and context.scene.name != task["parent"]["name"]:
        context.scene.name=task["parent"]["name"]
    elif len(bpy.data.scenes)>1 and not task["parent"]["name"] in bpy.data.scenes:
        film_scene=bpy.data.scenes.new(task["parent"]["name"])
        context.window.scene = film_scene
    if not context.scene.sequence_editor:
        context.scene.sequence_editor_create()

    #(GET SEQUENCER)
    #( -- get sequencer)
    if 'Video Editing' in bpy.data.workspaces:
        bpy.context.window.workspace = bpy.data.workspaces['Video Editing']
    area=None
    for area in bpy.context.window.workspace.screens[0].areas:
        if area.type == "SEQUENCE_EDITOR":
            break
    if not area:
        return(False, "No found SEQUENCE_EDITOR!")
    sequencer = context.scene.sequence_editor

    #(COPY FILE)
    b,r=path_utils.get_task_folder_path(task)
    if not b:
        return(False, r)
    #(-- new path)
    animatic_path=os.path.join(r, f'{path_utils.PREFIX_ANIMATIC}{task["parent"]["name"]}{os.path.splitext(filepath)[1]}')
    #(-- copy)
    shutil.copyfile(filepath, animatic_path)

    #(ANIMATIC TO SEQUENCER)
    #(-- clear)
    for seq in sequencer.sequences:
        if seq.channel in (1,2):
            if seq.type=="MOVIE":
                sequencer.sequences.remove(seq)
            elif seq.type=="SOUND":
                sequencer.sequences.remove(seq)
            else:
                sequencer.sequences.remove(seq)
    #(-- insert)
    sequence=sequencer.sequences.new_movie(task["parent"]["name"], animatic_path, 1, 0, fit_method='ORIGINAL')
    sound=sequencer.sequences.new_sound(f'{task["parent"]["name"]}.sound', animatic_path, 2, 0)
    #(-- duration)
    context.scene.frame_start=0
    context.scene.frame_end=sequence.frame_duration
    #(-- settings)
    hooks._set_render_setting_for_playblast()

    return(True, "Animatic loaded!")


def _make_bg_images(context, path):
    """
    Создание Background Image c мувкой аниматика для камеры сцены, при отсутствии камеры создаст её.

    Parameters
    ----------
    context : bpy.context
        Контекст блендера
    path : str
        Путь до мувки.

    Returns
    -------
    None
    """
    #(camera)
    if not bpy.context.scene.camera:
        camera=None
        for ob in bpy.data.objects:
            if ob.type=="CAMERA":
                camera=bpy.context.scene.camera=ob
        if not camera:
            camera_data=bpy.data.cameras.new('Shot_camera')
            camera=bpy.data.objects.new("Shot_camera", camera_data)
            context.scene.collection.objects.link(camera)
            bpy.context.scene.camera=camera
    else:
        camera=bpy.context.scene.camera
        
    #(clip)
    clip=None
    for c in bpy.data.movieclips:
        if bpy.path.abspath(c.filepath).replace('/..','')==path: # ?
            clip=c
    if not clip:
        clip=bpy.data.movieclips.load(path, check_existing=True)
    print(f"INFO:{clip.filepath}")
    clip.frame_start=int(float(os.environ['CGRULER_B3D_CURRENT_PROJECT_FSTART']))
        
    #(background images)
    camera.data.show_background_images=True
    if camera.data.background_images:
        bg=camera.data.background_images[0]
    else:
        bg=camera.data.background_images.new()
    #(bg settings)
    bg.clip=clip
    bg.display_depth="FRONT"
    bg.source="MOVIE_CLIP"
    bg.alpha=0.5
    bg.scale=0.5
    bg.offset=(0.25,0.25)


def download_animatic_to_shot(context, task, use_prefix=True):
    """
    * Загрузка последней версии аниматика из фтрек в задачу шота(animatic_asset_name.mp4)
    * Создание, или обновление, с ним секвенции.
    * Установка параметров сцены.
    * Создание или обновление Background Image камеры.

    Parameters
    ----------
    context : bpy.Context
        Блендер контекст
    task : ftrack.Task
        Объект задачи
    use_prefix : bool optional
        Использовать или нет префикс **Animatic_** в названии скачиваемого видео.

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    #(DOWNLOAD FILE)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)
    
    try:
        download_path, animatic_ft = _download_last_assetversion_move(sess, None, startswith="Animatic_", shot_id=task['parent']['id'])
    except Exception as e:
        srvconn.closing_session(sess)
        return(False, str(e))

    #(COPY FILE)
    b,task_folder_path=path_utils.get_task_folder_path(task)
    if not b:
        return(b,task_folder_path)
    #(--)
    if use_prefix:
        prefix=path_utils.PREFIX_ANIMATIC
    else:
        prefix=''
    #(--)
    animatic_name=f"{prefix}{task['parent']['name']}"
    file_name=f"{animatic_name}.mp4"
    animatic_path=os.path.join(task_folder_path, file_name)
    shutil.copyfile(download_path, animatic_path)

    #(SEQUENCER)
    b,sequencer=_get_sequencer(context)
    if not b:
        return(b,sequencer)
    #(--)
    move_name=animatic_name
    sound_name=f"{move_name}.sound"
    #(-- clear sequences)
    for seq in sequencer.sequences:
        if seq.channel in (1,2) or seq.name in (move_name, sound_name):
            sequencer.sequences.remove(seq)
    #(-- insert sequences)
    fstart=float(os.environ["CGRULER_B3D_CURRENT_PROJECT_FSTART"])
    move=sequencer.sequences.new_movie(move_name, animatic_path, 1, fstart, fit_method='ORIGINAL')
    sound=sequencer.sequences.new_sound(sound_name, animatic_path, 2, fstart)
    #(-- settings)
    hooks._set_render_setting_for_playblast(fstart='ENV', fend='ENV')

    try:
        _make_bg_images(context, animatic_path)
    except Exception as e:
        srvconn.closing_session(sess)
        return(False, str(e))

    srvconn.closing_session(sess)
    return(True, "Animatic loaded")


def rename_sequences_markers(context) -> tuple:
    """Переименование маркеров на таймлайне аниматика под имена секвенций.

    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    #(get markers sorted by frame)
    IGNORE_NAMES=("shot",)
    sorted_markers=list()
    markers=bpy.context.scene.timeline_markers
    for mr in sorted(markers, key=lambda m: m.frame):
        if mr.name in IGNORE_NAMES:
            continue
        elif mr.frame<context.scene.frame_start or mr.frame>context.scene.frame_end:
            continue
        else:
            sorted_markers.append(mr)
    #(get episode num)
    ep_name=os.environ["CGRULER_B3D_CURRENT_ASSET_NAME"]
    ep_type=os.environ["CGRULER_B3D_CURRENT_ASSET_TYPE"]
    ep_mun=ep_name.replace(ep_type, '')
    
    d=1
    if not sorted_markers or sorted_markers[0].frame != context.scene.frame_start:
        d=2
        m_name=f"Sequence{ep_mun}_{1:03}"
        print(m_name)
        context.scene.timeline_markers.new(m_name, frame=context.scene.frame_start)
    for i, m in enumerate(sorted_markers):
        num=i+d
        m_name=f"Sequence{ep_mun}_{num:03}"
        m.name=m_name
        print(m_name)
        
    
    return(True, "Ok!")


def rename_shots_markers(context) -> tuple:
    """
    Переименование маркеров на таймлайне аниматика под имена шотов.
    
    .. note:: Требуется наличие маркеров секвенций, создаваемых в в :func:`rename_sequences_markers`
    
    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    SHOTS_STEP=10
    sequence_markers=list()
    all_markers=bpy.context.scene.timeline_markers
    for mr in sorted(all_markers, key=lambda m: m.frame):
        if mr.name.startswith("Sequence"):
            sequence_markers.append(mr)
            
    if not sequence_markers:
        return(False, "No found Sequence markers!")
            
    for I,seq_m in enumerate(sequence_markers):
        num=seq_m.name.replace("Sequence", '')
        shot_name=f'Shot{num}_{10:04}'
        print(f"{shot_name} - {seq_m.frame}")
        seq_m.name=shot_name
        
        all_sorted_ms=sorted(all_markers, key=lambda m: m.frame)
        i=2
        for m in all_sorted_ms:
            if m.name.startswith("Sequence"):
                continue
            elif m.frame<=seq_m.frame or (I<len(sequence_markers)-1 and m.frame>=sequence_markers[I+1].frame):
                continue
            else:
                shot_name=f'Shot{num}_{i*SHOTS_STEP:04}'
                m.name=shot_name
                i+=1
                print(f"{shot_name} - {m.frame}")
                continue
    
    return(True, "Ok!")


def create_shots_from_markers(context, step=False, template="3d shot") -> tuple:
    """
    Создание шотов по маркерам созданным в :func:`rename_shots_markers`
    
    Parameters
    ----------
    context : bpy.context
        context
    step : bool, int, optional
        Если False то для всех маркеров, если число - то создаётся количество равное числу, начиная с начала.
    template : str
        Именования набора задач, который будет использоваться при создании шота.
        
    Returns
    -------
    tuple
        (True, Ok) или (False, comment)
    """
    frame_start=bpy.context.scene.frame_start
    frame_end=bpy.context.scene.frame_end

    if step%2:
        step+=1

    all_markers=sorted(bpy.context.scene.timeline_markers, key=lambda m: m.frame)
    markers=list()
    for m in all_markers:
        if m.name.startswith("Shot"):
            markers.append(m)
    if not markers:
        return(False, "No found Shot markers!")
    if step and step<len(markers):
        pass
    else:
        step=len(markers)
    
    #(sess)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    m_to_remove=list()
    return_data=(True, "Ok")
    for i in range(step):
        try:
            m=markers[i]
            #(m_data)
            m_data=dict()
            if i%2:
                m_data["video_channel"]=3
                m_data["sound_channel"]=4
            else:
                m_data["video_channel"]=5
                m_data["sound_channel"]=6
            m_data["fstart"]=m.frame
            if i<len(markers)-1:
                m_data["fend"]=markers[i+1].frame - 1
            else:
                m_data["fend"]=context.scene.frame_end
            m_data["duration"]=m_data["fend"]-m_data["fstart"]

            #(shot)
            b,shot=srvconn.get_asset_by_name(m.name, "Shot", sess=sess, close_session=False)
            if not b:
                raise Exception(str(shot))
            if not shot:
                b,shot=srvconn.create_shot(m.name, template, m_data, sess=sess, close_session=False)
                if not b:
                    raise Exception(str(shot))
            else:
                shot["custom_attributes"]["fstart"]= m_data["fstart"]
                shot["custom_attributes"]["fend"]= m_data["fend"]
                sess.commit()
            if shot:
                print(f'{m.name} - {m_data} - {shot["id"]}')
            else:
                print(f'{m.name} - {m_data} - None')
            
            #(animatic to version)
            hooks._set_render_setting_for_playblast(fstart=m.frame, fend=m.frame+m_data["duration"])
            bpy.ops.render.opengl(animation=True, sequencer=True)
            path=bpy.path.abspath(context.scene.render.filepath)
            index=1000
            tech_task=None
            print("INFO: push video to task START")
            for task in shot["children"]:
                tech_task=task
                # if task['type']['name'] in srvconn.TASK_TYPE_PRIORITY:
                #     if srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])<index:
                #         index=srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])
                #         tech_task=task
                # else:
                #     tech_task=task
                # print(f"{task['name']} - tech: {tech_task}")
                b,r=srvconn.push_video_to_ftrack(task, path, "animatic", asset_type_name="Video", prefix="Animatic", sess=sess, close_session=False)
                if not b:
                    srvconn.closing_session(sess)
                    raise Exception(str(r))
                break
            print("INFO: push video to task END")

            #(animatic to sequencer)
            #(--video to task folder)
            print(f"INFO: tech_task is \"{tech_task}\"")
            b,task_folder=path_utils.get_task_folder_path(tech_task)
            if not b:
                srvconn.closing_session(sess)
                raise Exception(str(task_folder))
            animatic_path=os.path.join(task_folder, f"{shot['name']}.mp4")
            shutil.copyfile(path, animatic_path)
            print(f"INFO: File is copied to {animatic_path}")
            #(-- to sequencer)
            sequencer = context.scene.sequence_editor
            sequence=sequencer.sequences.new_movie(shot['name'], animatic_path, m_data["video_channel"], m.frame, fit_method='ORIGINAL')
            sound=sequencer.sequences.new_sound(f'{shot["name"]}.sound', animatic_path, m_data["sound_channel"], m.frame)
            sound.mute=True
            print("INFO: Sequence is created")
            m_to_remove.append(m)
        except Exception as e:
            srvconn.closing_session(sess)
            return_data=(False, f'in create_shots_from_markers: "{e}" by marker "{m.name}"')

    #(fin)
    srvconn.closing_session(sess)

    bpy.context.scene.frame_start=frame_start
    bpy.context.scene.frame_end=frame_end

    for m in m_to_remove:
        bpy.context.scene.timeline_markers.remove(m)

    bpy.ops.wm.save_as_mainfile()
        
    return return_data


def re_create_selected_shots(context):
    """
    Пересоздание выделенных шотов по аниматику \
    (для случаев, когда подгружен отредактированный аниматик).

    Parameters
    ----------
    context : bpy.context
        Контекст блендера.

    Returns
    -------
    tuple
        (True, (tuple names of shots,)) или (False, comment)

    """
    print("FTRACK INFO: re_create_selected_shots()")
    frame_start=bpy.context.scene.frame_start
    frame_end=bpy.context.scene.frame_end
    #
    shots=_get_selected_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    sequencer = context.scene.sequence_editor
    return_data=(True, shots)
    for shot_name in shots:
        try:
            #(GET SEQUENCES of shot)
            #(--picture)
            if not shot_name in context.scene.sequence_editor.sequences_all:
                print(f"FTRACK INFO: Shot by that name \"{shot_name}\" does not exist!")
                continue
            shot=context.scene.sequence_editor.sequences_all[shot_name]
            #(--sound)
            sound_name=f"{shot_name}.sound"
            if not sound_name in context.scene.sequence_editor.sequences_all:
                print(f"FTRACK INFO: Sound by that name \"{sound_name}\" does not exist!")
                shot_sound=None
            else:
                shot_sound=context.scene.sequence_editor.sequences_all[sound_name]
            #(--hide sequences)
            shot.mute=True
            if shot_sound:
                shot_sound.mute=True

            #(M_DATA)
            m_data=dict()
            m_data["video_channel"]=shot.channel
            if shot_sound:
                m_data["sound_channel"]=shot_sound.channel
            else:
                m_data["sound_channel"]=m_data["video_channel"]+1
            m_data["fstart"]=shot.frame_start
            m_data["fend"]=shot.frame_final_end-1
            m_data["duration"]=shot.frame_duration
            # print(m_data)
            # continue

            #(HIDE SEQUENCES)
            shot_sound.mute=True
            shot.mute=True

            #(SHOT)
            b,shot_ft=srvconn.get_asset_by_name(shot_name, "Shot", sess=sess, close_session=False)
            if not b:
                raise Exception(str(shot_ft))
            if not shot_ft:
                raise Exception(f"Shot named \"{shot_name}\" does not exist on the server!")
            else:
                shot_ft["custom_attributes"]["fstart"]= m_data["fstart"]
                shot_ft["custom_attributes"]["fend"]= m_data["fend"]
                sess.commit()

            #(ANIMATIC TO VERSION)
            print(m_data)
            # hooks._set_render_setting_for_playblast(fstart=shot.frame_start, fend=m_data["fend"])
            hooks._set_render_setting_for_playblast()
            bpy.context.scene.frame_start=m_data["fstart"]
            bpy.context.scene.frame_end=m_data["fend"]
            bpy.ops.render.opengl(animation=True, sequencer=True)
            path=bpy.path.abspath(context.scene.render.filepath)
            index=1000
            # tech_task=None
            # for task in shot_ft["children"]:
            #     if task['type']['name'] in srvconn.TASK_TYPE_PRIORITY:
            #         if srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])<index:
            #             index=srvconn.TASK_TYPE_PRIORITY.index(task['type']['name'])
            #             tech_task=task
            #     # (--push video)
            #     b,r=srvconn.push_video_to_ftrack(task, path, "animatic", asset_type_name="Video", prefix="Animatic", sess=sess, close_session=False)
            #     if not b:
            #         raise Exception(str(r))
            tech_task=task=shot_ft["children"][0]
            b,r=srvconn.push_video_to_ftrack(task, path, "animatic", asset_type_name="Video", prefix="Animatic", sess=sess, close_session=False)
            if not b:
                raise Exception(str(r))


            #(ANIMATIC TO SEQUENCER)
            #(--video to task folder)
            b,task_folder=path_utils.get_task_folder_path(tech_task)
            if not b:
                raise Exception(str(task_folder))
            animatic_path=os.path.join(task_folder, f"{shot_name}.mp4")
            shutil.copyfile(path, animatic_path)
            #(-- CLEAR SEQUENCES)
            sequencer.sequences.remove(shot)
            sequencer.sequences.remove(shot_sound)
            #(--to sequencer)
            sequence=sequencer.sequences.new_movie(shot_name, animatic_path, m_data["video_channel"], m_data["fstart"], fit_method='ORIGINAL')
            sound=sequencer.sequences.new_sound(f'{shot_name}.sound', animatic_path, m_data["sound_channel"], m_data["fstart"])
            sound.mute=True
        except Exception as e:
            srvconn.closing_session(sess)
            return_data=(False, f'in re_create_selected_shots: "{e}" by shot "{shot_name}"')

    #(fin)
    srvconn.closing_session(sess)
    bpy.context.scene.frame_start=frame_start
    bpy.context.scene.frame_end=frame_end
    return return_data


def download_review(context) -> tuple:
    """Загрузка в секвенсор последней версии плейбласта ревью.

    Parameters
    ----------
    context : bpy.context
        Контекст блендера
    
    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)

    """
    #(last Review)
    shots=_get_selected_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    try:
        b,sequencer=_get_sequencer(context)
        if not b:
            raise Exception(str(sequencer))

        for shot_name in shots:
            try:
                #(shot)
                if not shot_name in sequencer.sequences_all:
                    print(f"FTRACK INFO: Shot by that name \"{shot_name}\" does not exist!")
                    continue
                shot=sequencer.sequences_all[shot_name]
                download_path, review_ft =_download_last_assetversion_move(sess, shot_name)
                            
                #(Copy file to task folder)
                b,task_folder=path_utils.get_task_folder_path(review_ft['task'])
                if not b:
                    raise Exception(str(f"{task_folder}"))
                review_path=os.path.join(task_folder, f"Review_{shot_name}.mp4")
                shutil.copyfile(download_path, review_path)

                #(review)
                review_name=f"{shot_name}.review"
                #(--clear)
                if review_name in sequencer.sequences_all:
                    sequencer.sequences.remove(sequencer.sequences_all[review_name])
                sequence=sequencer.sequences.new_movie(
                    review_name,
                    review_path,
                    shot.channel+5,
                    shot.frame_start,
                    fit_method='ORIGINAL')
                print(f"INFO: download review to {shot_name}")
            except Exception as e:
                print(f"WARNING: in {shot_name} {e}")
                continue
    except Exception as e:
        srvconn.closing_session(sess)
        return(False, str(e))

    srvconn.closing_session(sess)
    return(True, "Ok!")


def download_shot_animatic_to_episode(context):
    """Загрузка в секвенсор последней версии аниматика шота.

    * загружает для выделенных секвенций аниматиков.
        * при отсутствии на компьютере видеофайлов, мувки остаются пустые, но их можно выделить.
    * нужно для случая когда аниматики нарезались на другом компьютере.

    Parameters
    ----------
    context : bpy.context
        Контекст блендера
    
    Returns
    -------
    tuple
        (True, "Ok") или (False, comment)
    """
    #(last Review)
    shots=_get_selected_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    try:
        b,sequencer=_get_sequencer(context)
        if not b:
            raise Exception(str(sequencer))

        for shot_name in shots:
            #(shot)
            if not shot_name in sequencer.sequences_all:
                print(f"FTRACK INFO: Shot by that name \"{shot_name}\" does not exist!")
                continue
            shot=sequencer.sequences_all[shot_name]
            download_path, animatic_ft =_download_last_assetversion_move(sess, shot_name, startswith="Animatic_")
                        
            #(Copy file to task folder)
            save_path=os.path.join(
                os.environ['CGRULER_B3D_PROJECTS_DIR'],
                os.environ['CGRULER_B3D_CURRENT_PROJECT_ID'],
                os.path.basename(os.path.dirname(shot.filepath)),
                os.path.basename(shot.filepath))
            b,r=path_utils._create_path(os.path.dirname(save_path), True)
            if not b:
                raise Exception(str(r))
            shutil.copyfile(download_path, save_path)
            #(update)
            shot.update()
            sound_name=f'{shot_name}.sound'
            if sound_name in sequencer.sequences_all:
                sequencer.sequences.remove(sequencer.sequences_all[sound_name])
            sound=sequencer.sequences.new_sound(sound_name, save_path, shot.channel+1, shot.frame_start)
            sound.mute=True
            #(log)
            print(f"INFO: download animatic to {shot_name}")

    except Exception as e:
        srvconn.closing_session(sess)
        return(False, str(e))

    srvconn.closing_session(sess)
    return(True, "Ok!")


def select_shot_sequences_by_task_status(context, status_name: str):
    """
    Выделяет все секвенции шотов в **Sequence Editor**, статусы задач последних версий ревью которых соответствуют ``status_name``.
    """
    #(SHOTS)
    shots=_get_all_shots(context)
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    #(DESELECT)
    for sec in context.scene.sequence_editor.sequences_all:
        sec.select=False

    cash=os.environ.get('CGRULER_B3D_SHOTS_STATUSES')
    from_cash=False
    if cash:
        cash=json.loads(cash)
        delta=datetime.datetime.now() - datetime.datetime.fromisoformat(cash['time'])
        if delta<=datetime.timedelta(minutes=3):
            from_cash=True

    if from_cash:
        for shot_name in shots:
            if shot_name in cash['data'] and cash['data'][shot_name] == status_name:
                for ob in _get_shot_sequences(context, shot_name):
                    ob.select=True

    else:
        statuses=dict()
        for shot_name in shots:
            try:
                b,shot_ft=srvconn.get_asset_by_name(shot_name, "Shot", sess=sess, close_session=False, f=True)
                if not b:
                    raise Exception(str(shot_ft))
                if not shot_ft:
                    raise Exception(str(f"Shot named \"{shot_name}\" does not exist on the server!"))

                b,status=srvconn.get_status_name_of_last_review(shot_ft['id'], sess=sess, close_session=False, f=True)
                if not b:
                    raise Exception(str(status))

                print(f"INFO: {shot_name} - {status}")
                statuses[shot_name]=status

                if status == status_name:
                    for ob in _get_shot_sequences(context, shot_name):
                        ob.select=True

            except Exception as e:
                print(f"WARNING: {e}")
                continue
        os.environ['CGRULER_B3D_SHOTS_STATUSES']=json.dumps({
            "time": datetime.datetime.now().isoformat(),
            "data": statuses
            })

    srvconn.closing_session(sess)
    return(True, "Ok!")


def open_last_version_by_webbrowser(prefix, asset_name=False):
    """
    Запускает страницу обзора последнего ревью или коммита задачи в браузере.

    Parameters
    ----------
    prefix : str
        Значение из [`Review_`, `Commit_`]
    asset_name : str optional
        Имя ассета (пока только **Shot**), если не передавать будет использован id ассета из :ref:`CGRULER_B3D_CURRENT_ASSET_ID`

    Returns
    -------
    None
    """
    #path=https://propls.ftrackapp.com/#itemId=overview&objectType=show&slideEntityId=6790a903-e7ed-4168-9488-a6a9bbf1b6a7&slideEntityType=assetversion
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    #get_asset_by_name(name, type_name, sess=False, close_session=True, f=False)
    if not asset_name:
        asset_id=os.environ['CGRULER_B3D_CURRENT_ASSET_ID']
    else:
        b,asset=srvconn.get_asset_by_name(asset_name, "Shot", sess=sess, close_session=False)
        if not b:
            return(b, asset)
        asset_id=asset["id"]

    #(SEVER URL)
    server_url=settings.read_auth_data()['server_url']
    b,asset=srvconn.get_reviews_list_of_asset(asset_id, last=True, sess=sess, close_session=False, startswith=prefix)
    if not b:
        return(b,asset)
    webbrowser.open_new_tab(f"{server_url}/#itemId=overview&objectType=show&slideEntityId={asset['id']}&slideEntityType=assetversion")

    rdata=asset['id']
    sess.close()
    return(True, f"Opened webbrowser of asset {rdata}")


def change_status_of_selected_shot(shot_name, status_name) -> tuple:
    """
    Меняет статус последнего ревью и статус задачи этого ревью для выделенного в секвенсоре шота.
    

    Parameters
    ----------
    shot_name : str
        Имя шота для которого меняется статус.
    status_name : str
        Имя статуса

    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    #(SESS)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)

    r_data=(True, "Ok!")
    try:
        #(ASSET)
        b,asset=srvconn.get_asset_by_name(shot_name, "Shot", sess=sess, close_session=False)
        if not b:
            raise Exception(str(asset))
        #(REVIEW)
        b,review=srvconn.get_reviews_list_of_asset(asset["id"], last=True, sess=sess, close_session=False, startswith="Review_")
        if not b:
            raise Exception(str(review))

        status = sess.query(f'Status where name = "{status_name}"').one()
        
        review["status"]=status
        review["task"]["status"]=status

        sess.commit()


    except Exception as e:
        r_data=(False, str(e))

    sess.close()
    return r_data


def timing_from_selected_shots(context) -> tuple:
    """Выставляет границы сцены по пределам выделенных секвенций. 


    Returns
    -------
    tuple
        (True, "Ok!") или (False, comment)
    """
    fstart=None
    fend=None
    try:
        for seq in context.selected_sequences:
            if fstart is None or seq.frame_start<fstart:
                fstart=seq.frame_start
            if fend is None or fend<seq.frame_final_end:
                fend=seq.frame_final_end
        context.scene.frame_start=fstart
        context.scene.frame_end=fend
    except Exception as e:
        return(False, f"Problem in working.timing_from_selected_shots(): {e}")

    return(True, "Ok!")


def _make_task_data(task):
    """Создаёт словарь необходимых финальных данных из динамического объекта task.

    Parameters
    ----------
    task : ftrack.Task
        динамический объект задачи

    Returns
    -------
    dict
        созданный словарь
    """
    r_data=dict()
    r_data['id']=task['id']
    r_data['link']=task['link']
    r_data['name']=task['name']
    try:
        r_data['object_type']=task['object_type']['name']
    except:
        r_data['object_type']=None
    try:
        r_data['type']=task['type']['name']
    except:
        r_data['type']=None
    try:
        r_data['status']=task['status']['name']
    except:
        r_data['status']=None
    r_data['assignments'] = [ item['resource']['username'] for item in task['assignments']]
    r_data['custom_attributes']=dict(task['custom_attributes'])

    return r_data


def _make_version_data(version):
    """Создаёт словарь необходимых финальных данных из динамического объекта AssetVersion.

    Parameters
    ----------
    task : ftrack.AssetVersion
        динамический объект задачи

    Returns
    -------
    dict
        созданный словарь
    """
    r_data=dict()
    r_data['id']=version['id']
    r_data['comment']=version['comment']
    r_data['date']=version['date'].for_json()
    r_data['user']=version['user']['username']
    try:
        r_data['status']=version['status']['name']
    except:
        r_data['status']=None

    return r_data    


def backup(path, project, limit=False):
    """
    Создание бекапа активного проекта. Команада в блендер терминале:

    .. code-block:: python

        bpy.ops.ftrack.backup(path='/tmp/backup', limit=3)

    Parameters
    ----------
    path : str
        Путь до директории куда будет происходить загрузка.

    """
    DEPTH_DOWNLOADING={
        "Animation":30,
        "Location":50,
        "Character":10,
    }
    #(sess)
    b,sess=srvconn.get_session()
    if not b:
        return(b,sess)
    
    #(path)
    path_utils._create_path(path, True)
    
    #(project_data)
    #(-- get data)
    project_dir=os.path.join(path, os.environ["CGRULER_B3D_CURRENT_PROJECT_FULLNAME"])
    project_data=path_utils._backup_get_folder_data(project_dir)
    #(-- set links)
    project_data["links"]=list()
    for l in sess.query(f"select from, to from TypedContextLink where from.project.id={project['id']} and to.project.id={project['id']}"):
        project_data["links"].append((l['from']['id'], l['to']['id']))
    #(-- get tasks)
    if not project_data.get('tasks'):
        project_data["tasks"]=dict()

    #(tasks)
    if limit:
        tasks=sess.query(f'Task where project.id={os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]} and status.name not_in ("Closed", "Not started") limit {limit}')
    else:
        tasks=sess.query(f'Task where project.id={os.environ["CGRULER_B3D_CURRENT_PROJECT_ID"]} and status.name not_in ("Closed", "Not started")')
    logging.info('get tasks')

    #(folders)
    timing = time.time()
    for num,t in enumerate(tasks):
        project_data["tasks"][t['id']]=_make_task_data(t)
        logging.info(f'[{num}] start downloading {t["parent"]["name"]}/{t["name"]}')
        b,task_folder_path=path_utils.get_task_folder_path(t, create=True, backup=True)
        if not b:
            raise Exception(task_folder_path)

        #(get data)
        data_path=os.path.join(task_folder_path, path_utils.BACKUP_FOLDER_DATA)
        data=path_utils._backup_get_folder_data(task_folder_path)
        logging.info(f"*** {data_path} ***")

        #(download animatic)
        try:
            version_date=data.get("animatic_version")
            download_path, ob_ft = _download_last_assetversion_move(sess, None, startswith="Animatic_", shot_id=t['parent']['id'], after=version_date)
            path_utils._backup_update_folder_data(task_folder_path, {'animatic_version': ob_ft['date'].for_json()})
        except Exception as e:
            logging.warning(f'{e}')
            logging.debug(f'{traceback.format_exc()}')
        else:
            shutil.copyfile(download_path, os.path.join(task_folder_path, f"Animatic_{t['parent']['name']}.mp4"))
            path_utils._remove_temp_file(download_path)

        #(download review)
        try:
            review_date=data.get("review_version")
            download_path, ob_ft = _download_last_assetversion_move(sess, None, startswith="Review_", shot_id=t['parent']['id'], after=review_date)
            path_utils._backup_update_folder_data(task_folder_path, {'review_version': ob_ft['date'].for_json()})
        except Exception as e:
            logging.warning(f'{e}')
            logging.debug(f'{traceback.format_exc()}')
        else:
            shutil.copyfile(download_path, os.path.join(task_folder_path, f"Review_{t['parent']['name']}.mp4"))
            path_utils._remove_temp_file(download_path)

        #(download task_data)
        #(-- depth)
        if not t.get("parent") or not t["parent"].get('type'):
            depth=1
        else:
            depth=DEPTH_DOWNLOADING.get(t["parent"]['type']['name'],1)
        #(-- versions folder)
        versions_folder=os.path.join(task_folder_path, path_utils.BACKUP_VERSIONS_FOLDER)
        path_utils._create_path(versions_folder, True)
        versions_data=path_utils._backup_get_folder_data(versions_folder)
        #(-- last task version)
        task_data=path_utils._backup_get_folder_data(task_folder_path)
        last_task_version= task_data.get('task_version')
        #(-- get versions)
        if last_task_version:
            queryset=sess.query(f"AssetVersion where task.id = {t['id']} and asset.name like \"Commit_%\" and date>\"{last_task_version}\" order by date desc limit {depth}")
        else:
            queryset=sess.query(f"AssetVersion where task.id = {t['id']} and asset.name like \"Commit_%\" order by date desc limit {depth}")
        v_data=list()
        versions=queryset.all()
        versions.reverse()
        #(-- download versions)
        if not versions:
            logging.warning('No found new Commits_')
        else:
            for v in versions:
                #(-- download)
                b,url=srvconn.get_url_of_task_data_component(v['id'], sess=sess, close_session=False)
                if not b:
                    logging.warning(url)
                    continue
                zip_name=f"{srvconn.TASK_DATA_COMPONENT}{srvconn.TASK_DATA_EXT}"
                v_folder=os.path.join(versions_folder, v['id'])
                path_utils._create_path(v_folder, True)
                download_path=os.path.join(v_folder, zip_name)
                b,download_path=cgruler._download_file_from_url(url, zip_name, download_path=download_path)
                if not b:
                    logging.warning(download_path)
                    continue

                #(-- journal)
                v_data.append(_make_version_data(v))
                path_utils._backup_update_folder_data(task_folder_path, {'task_version': v['date'].for_json()})
            #(-- journal)
            path_utils._backup_update_folder_data(versions_folder, v_data)
            logging.info('downloaded Commits_')

    #(set project data)
    path_utils._backup_update_folder_data(project_dir, project_data)

    delta=time.time() - timing
    logging.info(F'All time: {delta/60} min')
    sess.close()
    return True, f"Tasks loaded to \"{path}\"!"