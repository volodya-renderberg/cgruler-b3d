.. _pycgruler-page:

pycgruler
=========

.. toctree::
   :maxdepth: 2

   pycgruler/cgruler
   pycgruler/config
   pycgruler/db
   pycgruler/environment_variables
   pycgruler/path_utils
   pycgruler/settings
   pycgruler/srvconn
   pycgruler/urls