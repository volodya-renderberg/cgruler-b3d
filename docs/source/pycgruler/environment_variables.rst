.. _environment-variables-page:

Переменные окружения
=====================


CGRULER_B3D_BACKUP_ROOT
-----------------------

* Путь до директории переданной для загрузки данных задач с фтрек на локаль.


.. _CGRULER_B3D_ROLE:

CGRULER_B3D_ROLE
-----------------

* Роль юзера: ``Working`` - исполнитель или ``Cheking`` - проверяющий.


.. _CGRULER_B3D_PROJECTS_DIR:

CGRULER_B3D_PROJECTS_DIR
------------------------


* Путь до текущей директрии для размещения проектов, определяемая в :func:`settings.set_projects_folder`



.. _CGRULER_B3D_CURRENT_STUDIO_ID:

CGRULER_B3D_CURRENT_STUDIO_ID
-----------------------------

* ``id`` - студии текущего проекта.


.. _CGRULER_B3D_CURRENT_STUDIO_NAME:

CGRULER_B3D_CURRENT_STUDIO_NAME
-------------------------------

* ``name`` - студии текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_ID:

CGRULER_B3D_CURRENT_PROJECT_ID
------------------------------

* ``id`` текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_NAME:

CGRULER_B3D_CURRENT_PROJECT_NAME
--------------------------------

* ``name`` текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_FULLNAME:

CGRULER_B3D_CURRENT_PROJECT_FULLNAME
------------------------------------

* ``full_name`` текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_FPS:

CGRULER_B3D_CURRENT_PROJECT_FPS
-------------------------------

* ``fps`` текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_WIDTH:

CGRULER_B3D_CURRENT_PROJECT_WIDTH
---------------------------------

* ``width`` ширина в пикселах для разрешения текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_HEIGHT:

CGRULER_B3D_CURRENT_PROJECT_HEIGHT
----------------------------------

* ``height`` высота в пикселах для разрешения текущего проекта.


.. _CGRULER_B3D_CURRENT_PROJECT_FSTART:

CGRULER_B3D_CURRENT_PROJECT_FSTART
----------------------------------

* Кадр с которого будет начинаться анимация во всех шотах. По умолчанию 1000.


.. _CGRULER_B3D_CURRENT_TASK_ID:

CGRULER_B3D_CURRENT_TASK_ID
---------------------------

* ``id`` текущей задачи.


.. _CGRULER_B3D_CURRENT_ASSET_NAME:

CGRULER_B3D_CURRENT_ASSET_NAME
------------------------------

* ``name`` имя ассета текущей задачи.


.. _CGRULER_B3D_CURRENT_ASSET_TYPE:

CGRULER_B3D_CURRENT_ASSET_TYPE
------------------------------

* ``object_type.name`` имя типа ассета текущей задачи.


.. _CGRULER_B3D_CURRENT_ASSET_ID:

CGRULER_B3D_CURRENT_ASSET_ID
----------------------------

* ``id`` ассета текущей задачи.


.. _CGRULER_B3D_AUTH_USER:

CGRULER_B3D_AUTH_USER
---------------------

* ``username`` авторизованного пользователя.


.. _CGRULER_B3D_SCENE_FEND:

CGRULER_B3D_SCENE_FEND
----------------------

* финальный кадр шота, считается от нуля. Определяется только для тех задач у которых тип родителя ``Shot``.


.. _CGRULER_B3D_CONTAINER_TYPES:

CGRULER_B3D_CONTAINER_TYPES
---------------------------

* сериализованный *json* словарь типов контейнеров текущей студии с ключами по ``id``. \
Заполняется в :func:`pycgruler.cgruler.project_set_current()`


.. _CGRULER_B3D_STATUSES_TYPES:

CGRULER_B3D_STATUSES_TYPES
---------------------------

* сериализованный *json* словарь типов статусов с ключами по ``id``. \
Заполняется в :func:`pycgruler.cgruler.project_set_current()`


.. _CGRULER_B3D_WORKING_STATUSES:

CGRULER_B3D_WORKING_STATUSES
----------------------------

* сериализованный *json* словарь статусов текущей студии с ключами по ``name``. \
Заполняется в :func:`pycgruler.cgruler.project_set_current()`

.. _CGRULER_B3D_SHOTS_STATUSES:

CGRULER_B3D_SHOTS_STATUSES
--------------------------

* сериализованный *json* словарь с ключами:
    * ``time`` - время записи в isoformat(), 
    * ``data`` - словарь статусов по именам шотов. Это статусы задач последних версий ревью шотов. 

Заполняется в :func:`working.select_shot_sequences_by_task_status`