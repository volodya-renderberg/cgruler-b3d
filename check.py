# -*- coding: utf-8 -*-

"""Проверки."""

import os
import json

import bpy

try:
    from pycgruler import path_utils
except:
    from .pycgruler import path_utils

CHECK_SHADING_STATUSES=(
    "modelling",
    "rigging",
    "layout",
    "shading",
)

def _path_test(textures_folder, tested_path) -> bool:
    if tested_path in ('', '/'):
        return False
    elif not os.path.exists(tested_path):
        return False
    elif os.path.samefile(textures_folder, tested_path):
        return True
    else:
       return _path_test(textures_folder, os.path.dirname(tested_path))

def _ascii_test(name) -> bool:
    return all(tuple(map(lambda x: True if ord(x)<128 else False, name)))

def check(task, context) -> tuple:
    """Проверки в соответствии с типом задачи. """
    ##
    all_right=True
    uncollected_textures=[] # текстуры не собранные в textures
    no_ascii=[] # объекты содержащие не ascii символы в именовании
    exists_asset_collections=False # наличие коллекций имена которых начинаются с имени ассета.
    ##
    test_data={
    "Uncollected textures":uncollected_textures,
    "Not ascii names": no_ascii,
    "Exists collections with asset name": exists_asset_collections
    }
    print("*** Check file ***")

    #(ASSET MODEL)
    if json.loads(os.environ["CGRULER_B3D_CONTAINER_TYPES"])[str(task['parent']["type"])]["name"]=="AssetModel":
        #(uncollected_textures)
        print("* Uncollected textures:")
        if task['process']['name'] in CHECK_SHADING_STATUSES:
            b, textures = path_utils.get_folder_of_textures_path(task)
            if not b:
                return(b, textures)

            for img in bpy.data.images:
                if img.packed_file:
                    continue
                elif img.name=="Render Result":
                    continue
                else:
                    b = _path_test(textures, img.filepath_from_user())
                    if not b:
                        uncollected_textures.append(img.__repr__())
                        all_right=False
                        print(f"\t* {img.name}: {img.filepath}")

        #(ascii)
        print("* Not ascii names:")
        testing_collections=(bpy.data.objects, bpy.data.meshes, bpy.data.collections, bpy.data.armatures, bpy.data.materials)
        for collection in testing_collections:
            for ob in collection:
                if not _ascii_test(ob.name):
                    ob_string=ob.__repr__()
                    no_ascii.append(ob_string)
                    all_right=False
                    print(f"\t* {ob_string}")

        #(exists_asset_collections)
        for collection in bpy.data.collections:
            if collection.name.startswith(task['parent']['name']):
                exists_asset_collections=True
                break
        if not exists_asset_collections:
            exists_asset_collections="No Found"
            test_data["Exists collections with asset name"]=exists_asset_collections
            all_right=False
        else:
            test_data["Exists collections with asset name"]=False

        print(f"* Exists collections with asset name - {exists_asset_collections}")
        print(f"* All check - {all_right}\n")

    return(all_right, test_data)